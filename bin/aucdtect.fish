# otherwise auCDtect won't be able to read music titles in russian
set -x LANG ru_RU.UTF-8
set starting_dir (pwd)
for dir in (fd -td '^\(\d{4}-\d{2}-\d{2}\) ' | path resolve)
    cd $dir
    for i in *.flac
        ffmpeg -i $i $i.wav -hide_banner -loglevel error
    end
    # iconv is used to change encoding to appropriate, as auCDtect spits weird letters instead of russian
    pwd
    fish -c "wine ~/.local/bin/auCDtect.exe -v -d -mS0 '*.wav' \
            | iconv -f RUSCII -t utf-8 \
            | tee auCDtect.txt" &
end
cd $starting_dir
