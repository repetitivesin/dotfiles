#!/usr/bin/env fish

for d in (fd -td '^\(\d{4}-\d{2}-\d{2}\)')
    printf "%s^\n" $d
    fd . -emp3 $d -x fish -c \
        'ffprobe -v quiet -print_format json -show_format "{}" \
            | jq "(.format.duration | tonumber) + 0.5 | floor"' \
        | awk '{s+=$1} END {print s}' \
        | xargs -I{} date -d @{} -u +%T
end
