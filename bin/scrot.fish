#!/usr/bin/env fish

if test -z "$XDG_PICTURES_DIR"
    set -g XDG_PICTURES_DIR "$HOME/Pictures"
end

set -g SCREENSHOT_DIR "$XDG_PICTURES_DIR/scrot"
set -g FILE_NAME "$SCREENSHOT_DIR/$(date -Ins).png"
set -g OPT_STRING "-t png" $FILE_NAME

if test ! -d "$SCREENSHOT_DIR"
    mkdir -p $SCREENSHOT_DIR
    or printf "Failed to create directory %s" $SCREENSHOT_DIR >&2
    and exit 1
end

argparse h/help 't/target=' -- $argv

if test -n "$_flag_h"
    printf "\
Usage: $(status filename) [-h|--help] <-t|--Target TARGET>
  -t --target
    TARGET is either:
      s|screen -- capture current output [default]
      w|window -- capture only selected window
      i|interactive -- use slurp for selecting region
  -h --help
    Print help text.

\
"
    exit 1
end

if test -n "$_flag_t"
    switch $_flag_t
        # Capture whole window
        case w window
            set -l region (begin
                if [ $XDG_CURRENT_DESKTOP = Hyprland ]
                    hyprctl activewindow -j | jq -r '. | "\(.at[0]),\(.at[1]) \(.size[0])x\(.size[1])"'
                else if [ $XDG_CURRENT_DESKTOP = sway ]
                    swaymsg -t get_tree | jq -r '.. | select(.type?) | select(.focused).rect | "\(.x),\(.y) \(.width)x\(.height)"'
                end
            end)

            set -p OPT_STRING "-g '$region'"

            # Capture whole output
        case s screen
            set -l screen (begin
                if [ $XDG_CURRENT_DESKTOP = Hyprland ]
                    hyprctl monitors -j | jq -r '.[] | select(.focused == true) | .name'
                else if [ $XDG_CURRENT_DESKTOP = sway ]
                    swaymsg -t get_outputs | jq -r '.[] | select(.focused) | .name'
                end
            end)
            set -p OPT_STRING "-o '$screen'"

            # Capture output chosen with slurp
        case i interactive
            set -l region (slurp || exit)
            set -p OPT_STRING "-g '$region'"
    end
end

set -p OPT_STRING grim
eval $OPT_STRING
wl-copy -t text/uri-list "file://$FILE_NAME"
printf "%s" $FILE_NAME
