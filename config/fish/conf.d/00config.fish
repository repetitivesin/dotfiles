# vi mode for fish
fish_hybrid_key_bindings

set st ~/Documents/Sync/
set -Ux ZK_NOTEBOOK_DIR "$st/zk/"

status --is-interactive; and begin
    # Multidot abbreviation ... expands to ../.., .... to ../../.., so on
    function multicd
        printf "cd %s\n" (string repeat -n (math (string length -- $argv[1]) - 1) ../)
    end
    abbr --add dotdot --regex '^\.\.+$' --function multicd

    # sh-like !!
    function last_history_item
        printf "%s\n" $history[1]
    end
    abbr -a !! --position anywhere --function last_history_item

    set FLAKE_PATH "$HOME/dotfiles"
    abbr --add --global -- nfup "nix flake update --flake '$FLAKE_PATH'"
    abbr --add --global -- nhup "home-manager switch --flake '$FLAKE_PATH'"
    abbr --add --global -- nsup "nixos-rebuild switch --use-remote-sudo --flake '$FLAKE_PATH'"

    alias ll 'ls -lAh --sort=time'
    set -x FZF_ALT_C_COMMAND "fd -H -td \
        --exclude '.direnv' \
        --exclude '__pycache__' \
        --exclude '.git' \
        --exclude '.nix-profile' \
        --exclude '.local/share/containers'"
    set -x FZF_CTRL_T_COMMAND "fd -H \
        --exclude '.direnv' \
        --exclude '__pycache__' \
        --exclude '.git' \
        --exclude '.nix-profile'\
        --exclude '.local/share/containers'"

end
