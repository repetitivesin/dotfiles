function create-cf
  set dir "$argv[1]"
  if test -e $dir
    printf "File exists\n"
    return
  end
  mkdir $dir
  cd $dir
  printf '#include <bits/stdc++.h>
using namespace std;
#define watch(x) cout << __LINE__ << "| " << #x << " = " << x << endl
#define all(x) x.begin(), x.end()
#define rall(x) x.rbegin(), x.rend()
#define YES cout << "YES\\\\n"
#define NO cout << "NO\\\\n"

int main(int argc, char *argv[]) {
  // ios_base::sync_with_stdio(false), cin.tie(nullptr);
  cout << "Hello %s" << endl;
}' $dir > main.cpp
end
