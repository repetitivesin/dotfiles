function m
  argparse --min-args 1 --max-args 1 "h/help" "n/noclose" -- $argv
  or return

  nohup mpv $argv[1] </dev/null &>/dev/null&
  disown

  if test -n "$_flag_d"
    exit
  end
end
