local ht = require "haskell-tools"
local lsp_config = require "_.plugins.lsp"
local base_conf = lsp_config["ext-config"] {}
local hydra = require "hydra"
local buffer = vim.api.nvim_get_current_buf()

local hydra_hint = [[
^⠀⠀⢀⣤⣤⣤⣤⣀⠀⠀⠀⠀⠀⠀⠀⠀⠀^
^⠀⠀⢸⣿⣿⣿⣿⣿⣷⡀⠀⠀⠀⠀⠀⠀⠀^
^⠀⠀⠘⠉⠉⠙⣿⣿⣿⣷⠀⠀⠀⠀⠀⠀⠀^ _a_: run current codelens
^⠀⠀⠀⠀⠀⠀⢸⣿⣿⣿⣧⠀⠀⠀⠀⠀⠀^ _e_: run all codelenses
^⠀⠀⠀⠀⠀⠀⣼⣿⣿⣿⣿⣆⠀⠀⠀⠀⠀^ _s_: hoogle signature
^⠀⠀⠀⠀⠀⣼⣿⣿⣿⣿⣿⣿⡀⠀⠀⠀⠀^ _r_: reload ghci
^⠀⠀⠀⠀⣴⣿⣿⣿⠟⣿⣿⣿⣷⠀⠀⠀⠀^ _f_: current buffer ghci
^⠀⠀⠀⣰⣿⣿⣿⡏⠀⠸⣿⣿⣿⣇⠀⠀⠀^ _R_: restart HLS
^⠀⠀⢠⣿⣿⣿⡟⠀⠀⠀⢻⣿⣿⣿⡆⠀⠀^ _q_: quit repl
^⠀⢠⣿⣿⣿⡿⠀⠀⠀⠀⠀⢿⣿⣿⣷⣤⡄^ _<Esc>_: quit
^⢀⣾⣿⣿⣿⠁⠀⠀⠀⠀⠀⠈⠿⣿⣿⣿⡇^
]]

local haskell_hydra = {
  name = "Haskell tools",
  hint = hydra_hint,
  mode = "n",
  body = "<Leader>c",
  config = {
    color = "teal",
    invoke_on_body = true,
    hint = {
      position = "bottom",
      float_opts = { border = "rounded" },
    },
    buffer = buffer,
  },
  heads = {
    {
      "a",
      function()
        vim.lsp.codelens.run()
      end,
      { desc = "run codelens" },
    },
    {
      "e",
      function()
        ht.lsp.buf_eval_all()
      end,
      { desc = "run all codelenses" },
    },
    {
      "s",
      function()
        ht.hoogle.hoogle_signature()
      end,
      { desc = "hoogle signature" },
    },
    -- Toggle a GHCi repl for the current package
    {
      "r",
      function()
        ht.repl.reload()
      end,
      { desc = "reload repl" },
    },
    -- Toggle a GHCi repl for the current buffer
    {
      "f",
      function()
        ht.repl.toggle(vim.api.nvim_buf_get_name(0))
      end,
      { desc = "run repl for current buffer" },
    },
    {
      "R",
      function()
        ht.lsp.restart()
      end,
      { desc = "restart HLS" },
    },
    {
      "q",
      function()
        ht.repl.quit()
      end,
      { desc = "quit repl" },
    },
    {
      "<Esc>",
      nil,
      { exit = true },
    },
  },
}

hydra(haskell_hydra)

-- ht.start_or_attach {
--   hls = {
--     on_attach = function(client, buffer)
--       base_conf.on_attach(client, buffer)
--     end,
--     filetypes = { "cabal", "haskell", "lhaskell" },
--     settings = {
--       haskell = {
--         cabalFormattingProvider = "cabalfmt",
--         formattingProvider = "fourmolu",
--       },
--     },
--   },
-- }

-- Detect nvim-dap launch configurations
-- (requires nvim-dap and haskell-debug-adapter)
-- ht.dap.discover_configurations(bufnr)