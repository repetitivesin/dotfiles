vim.opt_local.spell = true
vim.opt_local.sw = 2
vim.opt_local.ts = 2
vim.opt_local.sts = 2
vim.opt_local.backupcopy = "yes"
vim.opt_local.wrap = false