-- local p = require "jit.p"
-- vim.api.nvim_create_autocmd("ExitPre", {
--   callback = function()
--     p.stop()
--   end,
-- })
-- p.start("-3vFm3", "profiler.log")

-- Bootstrap hotpot.nvim
local path_package = vim.fn.stdpath "data" .. "/site/"
local hotpot_path = path_package .. "pack/deps/start/hotpot.nvim"
if not vim.loop.fs_stat(hotpot_path) then
  vim.notify("Bootstrapping hotpot.nvim...", vim.log.levels.INFO)
  vim.fn.system {
    "git",
    "clone",
    "--filter=blob:none",
    "--single-branch",
    "https://github.com/rktjmp/hotpot.nvim.git",
    hotpot_path,
  }
end
vim.loader.enable()
require("hotpot").setup {
  provide_require_fennel = true,
  enable_hotpot_diagnostics = false,
  compiler = {
    modules = {
      correlate = true,
      useBitLib = true,
    },
  },
}

local mini_path = path_package .. "pack/deps/start/mini.nvim"
if not vim.loop.fs_stat(mini_path) then
  vim.cmd 'echo "Installing `mini.nvim`" | redraw'
  vim.fn.system {
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/echasnovski/mini.nvim",
    mini_path,
  }
  vim.cmd 'packadd mini.nvim | helptags ALL'
  vim.cmd 'echo "Installed `mini.nvim`" | redraw'
end

require("mini.deps").setup { path = { package = path_package } }

require("mini.deps").add { name = "hotpot.nvim" }

vim.opt.runtimepath:prepend { hotpot_path }

require "_"
require "_.plugins"