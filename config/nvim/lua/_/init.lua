local M = {}
local mapt = require("_.util").mapt

M["variables"] = {
  vimsyn_embed = "l",
  vimsyn_noerror = 0,
  mapleader = " ",
  maplocalleader = ",",
  loaded_perl_provider = 0,
  loaded_node_provider = 0,
  loaded_ruby_provider = 0,
  loaded_python3_provider = 0,
}

for k, v in pairs(M.variables) do
  vim.g[k] = v
end

M["options"] = {
  autoindent = true,
  cdhome = true,
  cmdheight = 1,
  completeopt = "menu,menuone,noselect",
  conceallevel = 2,
  cursorline = true,
  expandtab = true,
  foldcolumn = "0",
  foldenable = true,
  foldlevel = 99,
  foldmethod = "indent",
  foldopen = { "hor", "mark", "percent", "quickfix", "search", "tag", "undo" },
  formatoptions = "tqj",
  guifont = "FiraCode Nerd Font Mono:h10",
  hidden = true,
  ignorecase = true,
  incsearch = true,
  linebreak = true,
  matchpairs = "(:),[:],{:}",
  mouse = "a",
  number = true,
  scrolloff = 0,
  shiftwidth = 2,
  showcmd = true,
  signcolumn = "auto",
  smartcase = true,
  softtabstop = 2,
  spelllang = "en,pl,ru",
  splitbelow = true,
  splitright = true,
  tabstop = 2,
  termguicolors = true,
  textwidth = 80,
  undofile = true,
  updatetime = 1000,
  virtualedit = "block",
  wildmenu = true,
  laststatus = 3,
  winblend = 10,
  backup = false,
  hlsearch = false,
  lazyredraw = false,
}

for k, v in pairs(M.options) do
  vim.opt[k] = v
end

mapt({
  { "n",          "x",       "\"_dl" },
  { "x",          "x",       "\"_d" },
  { { "i", "c" }, "<C-d>",   "<Del>" },
  { { "n", "v" }, "Q",       "q:" },
  { "n",          "<C-p>",   "<Esc>@:" },
  { "v",          "<C-p>",   "<Esc>@:gv" },
  { "n",          "<Tab>",   "<Cmd>bn<CR>" },
  { "n",          "<S-Tab>", "<Cmd>bprevious<CR>" },
  { "i",          "<C-BS>",  "<C-w>" },
  { "t",          "<A-Esc>", "<C-\\><C-n>" },
  { "v",          ".",       ":norm .<CR>gv" },
}, { silent = true })

vim.api.nvim_create_augroup("hlhooks", { clear = true })
vim.api.nvim_create_autocmd("CmdlineEnter", {
  pattern = "[/\\?]",
  group = "hlhooks",
  callback = function()
    vim.opt.hlsearch = true
    return nil
  end,
})
vim.api.nvim_create_autocmd("CmdlineLeave", {
  pattern = "[/\\?]",
  group = "hlhooks",
  callback = function()
    vim.opt.hlsearch = (M.options.hlsearch or false)
    return nil
  end,
})

local au_copy = vim.api.nvim_create_augroup("copy-highlight", { clear = true })
vim.api.nvim_create_autocmd("TextYankPost", {
  pattern = "*",
  group = au_copy,
  callback = function()
    return vim.highlight.on_yank { timeout = 500 }
  end,
})

local au_term = vim.api.nvim_create_augroup("terminal", { clear = true })
vim.api.nvim_create_autocmd("TermOpen", {
  pattern = "*",
  group = au_term,
  callback = function()
    vim.opt_local["nu"] = false
    vim.opt_local["rnu"] = false
    vim.opt_local["signcolumn"] = "no"
  end,
})

if vim.g.started_by_firenvim then
  vim.opt.laststatus = 0
  vim.opt.showtabline = 1
  vim.opt.cmdheight = 0
  vim.opt.showmode = 0
  vim.opt.signcolumn = "no"
  vim.opt.textwidth = 0
  mapt { { "n", "j", "gj" }, { "n", "k", "gk" } }
end

return M