local cmp = require "cmp"
local nap_cmp = require "nvim-autopairs.completion.cmp"
local nap = require "nvim-autopairs"
local Rule = require "nvim-autopairs.rule"
local cond = require "nvim-autopairs.conds"

nap.setup {
  fast_wrap = {
    map = "<M-e>",
    chars = {
      "{",
      "[",
      "(",
      "'",
      "\"",
      "\\\"",
      "«",
      "“",
    },
    end_key = "$",
  },
  ignored_next_char = "[%w%.]",
  map_cr = true,
  check_ts = true,
  enable_check_bracket_line = false,
}

cmp.event:on("confirm_done", nap_cmp.on_confirm_done())

local function offset_rule(l, r, ft)
  ft = ft or {}
  return Rule((l .. " "), (" " .. r), ft)
    -- Don't insert pair, as it was already inserted by Rule(" ", " ")
    :with_pair(
      cond.none()
    )
    -- Don't delete the whole pair when only single space is deleted, just
    -- delete inner spaces
    :with_del(
      cond.none()
    )
    :with_move(function(opts)
      return nil ~= opts.prev_char:match(".%" .. r)
    end)
    :use_key(r)
end

local function basic_rule(l, r, ft)
  ft = ft or {}
  local rule = Rule(l, r, ft)
  rule:with_pair(cond.done())
  return rule:with_move(function(opts)
    return (opts.char == opts.rule.end_pair)
  end)
end

nap.add_rules {
  Rule(" ", " ", { "-org", "-markdown" }):with_pair(function(opts)
    local pair = opts.line:sub((opts.col - 1), opts.col)
    return vim.tbl_contains({ "()", "[]", "{}", "<>" }, pair)
  end),
  offset_rule("(", ")"),
  offset_rule("[", "]"),
  offset_rule("{", "}"),
  basic_rule("“", "”"),
  basic_rule("«", "»"),
}