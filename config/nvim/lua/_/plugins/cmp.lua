local cmp = require "cmp"
local lspkind = require "lspkind"
local types = require "cmp.types"
local util = require "_.util"

local function select_next(fallback)
  local luasnip = require "luasnip"
  if luasnip.choice_active() then
    return luasnip.change_choice(1)
  elseif cmp.visible() then
    return cmp.select_next_item { behavior = cmp.SelectBehavior.Insert }
  else
    return fallback()
  end
end

local function select_prev(fallback)
  local luasnip = require "luasnip"
  if luasnip.choice_active() then
    return luasnip.change_choice(-1)
  elseif cmp.visible() then
    return cmp.select_prev_item { behavior = cmp.SelectBehavior.Insert }
  else
    return fallback()
  end
end

local function confirm(fallback)
  if cmp.visible() then
    return cmp.confirm { behavior = cmp.ConfirmBehavior.Insert, select = true }
  else
    return fallback()
  end
end

local function is_parinfer_active()
  return (
    (vim.g.parinfer_enabled or vim.b.parinfer_enabled)
    and vim.tbl_contains(vim.g.parinfer_filetypes, vim.bo.ft)
  )
end

local function jump_next(fallback)
  local luasnip = require "luasnip"
  if luasnip.locally_jumpable(1) then
    return luasnip.jump(1)
  elseif is_parinfer_active() then
    return util.fk "<Plug>(parinfer-tab)"
  else
    return fallback()
  end
end

local function jump_prev(fallback)
  local luasnip = require "luasnip"
  if luasnip.locally_jumpable(-1) then
    return luasnip.jump(-1)
  elseif is_parinfer_active() then
    return util.fk "<Plug>(parinfer-backtab)"
  else
    return fallback()
  end
end

local function complete(fallback)
  if not cmp.visible() then
    return cmp.complete()
  else
    return fallback()
  end
end

local function close(fallback)
  if cmp.visible() then
    return cmp.close()
  else
    return fallback()
  end
end

local function not_started_by_firenvim()
  return not vim.g.started_by_firenvim
end

local function luasnip_expand(snip)
  return require("luasnip").lsp_expand(snip.body)
end

cmp.setup {
  enabled = not_started_by_firenvim,
  snippet = { expand = luasnip_expand },
  window = {
    completion = cmp.config.window.bordered(),
    documentation = cmp.config.window.bordered(),
  },
  confirmation = { default_behavior = types.cmp.ConfirmBehavior.Replace },
  mapping = {
    ["<C-d>"] = cmp.mapping.scroll_docs(4),
    ["<C-u>"] = cmp.mapping.scroll_docs(-4),
    ["<C-Space>"] = cmp.mapping(complete, { "i", "c" }),
    ["<C-e>"] = cmp.mapping(close, { "i", "c" }),
    ["<C-a>"] = cmp.mapping(confirm, { "i", "c" }),
    ["<C-n>"] = cmp.mapping(select_next, { "i", "c" }),
    ["<C-p>"] = cmp.mapping(select_prev, { "i", "c" }),
    ["<C-j>"] = cmp.mapping(select_next, { "i", "c", "s" }),
    ["<C-k>"] = cmp.mapping(select_prev, { "i", "c", "s" }),
    ["<Tab>"] = cmp.mapping(jump_next, { "i", "s" }),
    ["<S-Tab>"] = cmp.mapping(jump_prev, { "i", "s" }),
  },
  formatting = {
    format = lspkind.cmp_format {
      preset = "default",
      mode = "symbol",
      maxwidth = 50,
    },
  },
  sources = {
    { name = "nvim_lsp_signature_help" },
    { name = "nvim_lsp" },
    { name = "lazydev" },
    { name = "conjure" },
    { name = "luasnip" },
    {
      name = "buffer",
      option = { keyword_pattern = "\\k\\+" },
    },
    { name = "nvim_lua" },
    { name = "latex_symbols" },
    { name = "async_path", option = { strategy = 2 } },
  },
}

cmp.setup.filetype({ "TelescopePrompt" }, { enabled = false })

cmp.setup.cmdline(
  { "/", "?" },
  { sources = { { name = "buffer" } }, mapping = cmp.mapping.preset.cmdline() }
)

cmp.setup.cmdline(":", {
  sources = { { name = "cmdline" }, { name = "async_path" } },
  mapping = cmp.mapping.preset.cmdline(),
})