vim.g["conjure#mapping#prefix"] = "<LocalLeader>"
vim.g["conjure#mapping#doc_word"] = "d"
vim.g["conjure#highlight#enabled"] = true
vim.g["conjure#log#fold#enabled"] = true