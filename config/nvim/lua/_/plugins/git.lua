local neogit = require "neogit"
local gitsigns = require "gitsigns"
local hydra = require "hydra"

local hint = [[
 _J_: next hunk   _s_: stage hunk        _d_: show deleted   _b_: blame line
 _K_: prev hunk   _u_: undo last stage   _p_: preview hunk   _B_: blame show full ^
 _r_: reset hunk  _S_: stage buffer      _R_: reset buffer   _/_: show base file
 ^
 ^ ^              _<Enter>_: Neogit             _<Esc>_: exit]]

local function on_attach(buffer)
  local function on_enter()
    vim.cmd "mkview"
    vim.cmd "silent! %foldopen!"
    vim.bo["modifiable"] = false
    gitsigns.toggle_signs(true)
    gitsigns.toggle_linehl(true)
    gitsigns.toggle_deleted(true)
    gitsigns.toggle_numhl(true)
  end

  local function on_exit()
    local cursor_pos = vim.api.nvim_win_get_cursor(0)
    vim.cmd "silent! loadview"
    vim.api.nvim_win_set_cursor(0, cursor_pos)
    vim.cmd "normal! zv"
    gitsigns.toggle_signs(false)
    gitsigns.toggle_linehl(false)
    gitsigns.toggle_deleted(false)
    gitsigns.toggle_numhl(false)
  end

  local function prev_hunk()
    if vim.wo.diff then
      return "[c"
    else
      vim.schedule(function()
        return gitsigns.prev_hunk()
      end)
      return "<Ignore>"
    end
  end

  local function next_hunk()
    if vim.wo.diff then
      return "]c"
    else
      vim.schedule(function()
        return gitsigns.next_hunk()
      end)
      return "<Ignore>"
    end
  end

  local function reset_hunk()
    vim.bo["modifiable"] = true
    gitsigns.reset_hunk()
    vim.bo["modifiable"] = false
  end

  local function reset_buffer()
    vim.bo["modifiable"] = true
    gitsigns.reset_buffer()
    vim.bo["modifiable"] = false
  end

  local function blame_line()
    return gitsigns.blame_line { full = true }
  end

  hydra {
    name = "Git",
    hint = hint,
    mode = { "n", "x" },
    body = "<leader>g",
    config = {
      buffer = buffer,
      color = "pink",
      invoke_on_body = true,
      hint = { float_opts = { border = "rounded" } },
      on_key = function()
        return vim.wait(50)
      end,
      on_enter = on_enter,
      on_exit = on_exit,
    },
    heads = {
      { "J", next_hunk, { expr = true, desc = "next hunk" } },
      { "K", prev_hunk, { expr = true, desc = "prev hunk" } },
      { "r", reset_hunk, { desc = "reset hunk" } },
      { "R", reset_buffer, { desc = "reset buffer" } },
      { "s", ":Gitsigns stage_hunk<CR>", { desc = "stage hunk" } },
      { "S", gitsigns.stage_buffer, { desc = "stage buffer" } },
      { "u", gitsigns.undo_stage_hunk, { desc = "unstage lust hunk" } },
      { "p", gitsigns.preview_hunk, { desc = "preview hunk" } },
      {
        "d",
        gitsigns.toggle_deleted,
        { nowait = true, desc = "toggle deleted" },
      },
      { "b", gitsigns.blame_line, { desc = "blame" } },
      { "B", blame_line, { desc = "blame show full" } },
      { "/", gitsigns.show, { exit = true, desc = "show base file" } },
      {
        "<Enter>",
        "<Cmd>Neogit<CR>",
        { exit = true, desc = "Neogit", nowait = true },
      },
      { "<Esc>", nil, { exit = true, nowait = true, desc = "exit" } },
    },
  }
end

gitsigns.setup { on_attach = on_attach, signcolumn = false }

neogit.setup {
  use_telescope = true,
  telescope_sorter = function()
    return require("telescope").extensions.fzf.native_fzf_sorter
  end,
  integrations = { diffview = true },
}