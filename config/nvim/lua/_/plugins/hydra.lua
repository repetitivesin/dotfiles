local hydra = require "hydra"
local sidescroll_hydra = {
  name = "Side scroll",
  mode = "n",
  body = "z",
  config = { foreign_keys = nil, timeout = true },
  heads = {
    { "h", "5zh" },
    { "l", "5zl", { desc = "←/→" } },
    { "H", "zH" },
    { "L", "zL", { desc = "half screen ←/→" } },
  },
}
local option_hint = [[
             Options
  ^
  _v_ %{ve} virtual edit
  _i_ %{list} invisible characters
  _s_ %{spell} spell
  _w_ %{wrap} wrap
  _c_ %{cul} cursor line
  _n_ %{nu} number
  _r_ %{rnu} relative number

                           _<Esc>_]]

local function toggle_nu()
  vim.o.number = not vim.o.number
end
local function toggle_rnu()
  if vim.o.relativenumber then
    vim.o.relativenumber = false
  else
    vim.o.number = true
    vim.o.relativenumber = true
  end
end
local function toggle_vedit()
  if vim.o.virtualedit == "all" then
    vim.o.virtualedit = "block"
  else
    vim.o.virtualedit = "all"
  end
end
local function toggle_list()
  vim.o.list = not vim.o.list
end
local function toggle_spell()
  vim.o.spell = not vim.o.spell
end
local function toggle_wrap()
  if not vim.o.wrap then
    vim.o.wrap = true
  else
    vim.o.wrap = false
  end
end
local function toggle_cursorline()
  vim.o.cursorline = not vim.o.cursorline
end

local option_hydra = {
  name = "Options",
  hint = option_hint,
  mode = { "n", "x" },
  body = "<leader>o",
  config = {
    color = "amaranth",
    invoke_on_body = true,
    hint = { position = "middle", float_opts = { border = "rounded" } },
  },
  heads = {
    { "n", toggle_nu, { desc = "number" } },
    { "r", toggle_rnu, { desc = "relativenumber" } },
    { "v", toggle_vedit, { desc = "virtualedit" } },
    { "i", toggle_list, { desc = "show invisible" } },
    { "s", toggle_spell, { exit = true, desc = "spell" } },
    { "w", toggle_wrap, { desc = "wrap" } },
    { "c", toggle_cursorline, { desc = "cursor line" } },
    { "<Esc>", nil, { exit = true } },
  },
}

local exit_hint = [[
       Exit menu
  _q_uit        _Q_uit!
  quit _a_ll    quit _A_ll!
  save_x_quit   save_X_quit all ^
  _w_rite       _W_rite all
  ^         _<Esc>_]]

local cmd = require("hydra.keymap-util")["cmd"]

local exit_hydra = {
  name = "Exit menu",
  hint = exit_hint,
  mode = "n",
  body = "<Leader>q",
  config = {
    color = "teal",
    invoke_on_body = true,
    hint = { float_opts = { border = "rounded" } },
  },
  heads = {
    { "q", cmd "q" },
    { "Q", cmd "q!" },
    { "a", cmd "qa" },
    { "A", cmd "qa!" },
    { "x", cmd "x" },
    { "X", cmd "xa" },
    { "w", cmd "w", { exit = false } },
    { "W", cmd "wa", { exit = false } },
    { "<Esc>", nil, { exit = true } },
  },
}

local window_hint = [[
^  Window
^ _h_ _j_ _k_ _l_ ^
^ _H_ _J_ _K_ _L_ ^
^ _q_ _o_ _r_ _p_ ^
^ _v_ _s_ _=_ _n_ ^
^ ^^^ _<Esc>_]]

local window_hydra = {
  name = "Window menu",
  hint = window_hint,
  mode = "n",
  body = "<Leader>w",
  config = {
    color = "amaranth",
    invoke_on_body = true,
    hint = {
      type = "window",
      position = "bottom",
      float_opts = { border = "single" },
    },
  },
  heads = {
    { "h", "<C-w>h", {} },
    { "j", "<C-w>j", {} },
    { "k", "<C-w>k", {} },
    { "l", "<C-w>l", {} },
    { "H", "<C-w>H", {} },
    { "J", "<C-w>J", {} },
    { "K", "<C-w>K", {} },
    { "L", "<C-w>L", {} },
    { "q", "<C-w>q", {} },
    { "o", "<C-w>o", {} },
    { "r", "<C-w>r", {} },
    { "p", "<C-w>p", {} },
    { "v", "<C-w>v", {} },
    { "s", "<C-w>s", {} },
    { "=", "<C-w>=", {} },
    { "n", "<C-w>n", {} },
    { "<Esc>", nil, { exit = true } },
  },
}

hydra(exit_hydra)
hydra(sidescroll_hydra)
hydra(option_hydra)
hydra(window_hydra)