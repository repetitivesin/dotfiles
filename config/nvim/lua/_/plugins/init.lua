local MiniDeps = require "mini.deps"
local add = MiniDeps["add"]
local now = MiniDeps["now"]
local later = MiniDeps["later"]
local util = require "_.util"

now(function()
  add { source = "https://gitlab.com/repetitivesin/16cm.nvim.git" }
  local cm = require "16cm"
  cm.setup {
    persistence = { enabled = true },
    override = {
      MatchParen = { bold = true, underline = true, sp = "5", fg = "5" },
      ["@markup.heading.1"] = { bg = "0", fg = "A", bold = true },
      ["@markup.heading.2"] = { bg = "0", fg = "B", bold = true },
      ["@markup.heading.3"] = { bg = "0", fg = "C", bold = true },
      ["@markup.heading.4"] = { bg = "0", fg = "D", bold = true },
      ["@markup.heading.5"] = { bg = "0", fg = "E", bold = true },
      ["@markup.heading.6"] = { bg = "0", fg = "F", bold = true },
      ["@markup.heading.1.marker"] = { bg = "A", fg = "0" },
      ["@markup.heading.2.marker"] = { bg = "B", fg = "0" },
      ["@markup.heading.3.marker"] = { bg = "C", fg = "0" },
      ["@markup.heading.4.marker"] = { bg = "D", fg = "0" },
      ["@markup.heading.5.marker"] = { bg = "E", fg = "0" },
      ["@markup.heading.6.marker"] = { bg = "F", fg = "0" },
      RenderMarkdownH1Bg = { link = "@markup.heading.1.markdown" },
      RenderMarkdownH2Bg = { link = "@markup.heading.2.markdown" },
      RenderMarkdownH3Bg = { link = "@markup.heading.3.markdown" },
      RenderMarkdownH4Bg = { link = "@markup.heading.4.markdown" },
      RenderMarkdownH5Bg = { link = "@markup.heading.5.markdown" },
      RenderMarkdownH6Bg = { link = "@markup.heading.6.markdown" },
    },
  }
end)

add { name = "mini.nvim" }
require "_.plugins.mini"

add { source = "nvim-tree/nvim-web-devicons" }
add { source = "nvim-lua/plenary.nvim" }
add { source = "tpope/vim-repeat" }

now(function()
  add { source = "utilyre/sentiment.nvim" }
  require("sentiment").setup {
    pairs = {
      { "{", "}" },
      { "[", "]" },
      { "(", ")" },
      { "<", ">" },
    },
  }
end)

now(function()
  local function TSUpdate()
    vim.cmd ":TSUpdate"
  end
  add {
    source = "nvim-treesitter/nvim-treesitter",
    hooks = { post_checkout = TSUpdate },
  }
  add { source = "nvim-treesitter/playground" }
  require "_.plugins.treesitter"
end)

add { source = "Olical/conjure", checkout = "v4.53.0" }
require "_.plugins.conjure"

add { source = "lukas-reineke/indent-blankline.nvim" }
require("ibl").setup { exclude = { filetypes = { "markdown" } } }

add { source = "https://git.sr.ht/~whynothugo/lsp_lines.nvim" }
require("lsp_lines").setup {}
vim.diagnostic.config { virtual_text = false }

add {
  source = "neovim/nvim-lspconfig",
  depends = {
    "folke/lazydev.nvim",
    "nvimtools/hydra.nvim",
    "nvim-telescope/telescope.nvim",
    "simrat39/rust-tools.nvim",
    "hrsh7th/cmp-nvim-lsp",
  },
}
require "_.plugins.lsp"

add {
  source = "SmiteshP/nvim-navbuddy",
  depends = {
    "neovim/nvim-lspconfig",
    "SmiteshP/nvim-navic",
    "MunifTanjim/nui.nvim",
    "hrsh7th/cmp-nvim-lsp",
  },
}
require "_.plugins.navbuddy"

add {
  source = "nvimtools/none-ls.nvim",
  depends = {
    "ThePrimeagen/refactoring.nvim",
    "nvim-lua/plenary.nvim",
    "nvimtools/none-ls-extras.nvim",
  },
}
require "_.plugins.null-ls"

later(function()
  add { source = "catgoose/nvim-colorizer.lua" }
  require("colorizer").setup {
    filetypes = { "css", "javascript", "html", "scss" },
    user_default_options = {
      RGB = true,
      RRGGBB = true,
      RRGGBBAA = true,
      rgb_fn = true,
      hsl_fn = true,
      css = true,
      css_fn = true,
      mode = "background",
      names = false,
    },
  }
end)

later(function()
  add {
    source = "kevinhwang91/nvim-ufo",
    depends = {
      "kevinhwang91/promise-async",
      "nvim-treesitter/nvim-treesitter",
    },
  }
  require "_.plugins.ufo"
end)

add { source = "mbbill/undotree" }
util.map("n", "U", ":UndotreeToggle<CR>")

later(function()
  add { source = "monaqa/dial.nvim" }
  util.mapt {
    { { "n", "v" }, "<C-a>", "<Plug>(dial-increment)" },
    { { "n", "v" }, "<C-x>", "<Plug>(dial-decrement)" },
    { "v", "g<C-a>", "g<Plug>(dial-increment)", { remap = true } },
    { "v", "g<C-x>", "g<Plug>(dial-decrement)", { remap = true } },
  }
end)

add("folke/trouble.nvim", { depends = { "nvim-tree/nvim-web-devicons" } })
require("trouble").setup {}

later(function()
  add { source = "folke/zen-mode.nvim" }
  util.map({ "n", "t", "i" }, "<C-.>", "<Cmd>ZenMode<CR>")
  require("zen-mode").setup { window = { width = 1 } }
end)

add { source = "rcarriga/nvim-notify" }
require "_.plugins.notify"

later(function()
  add { source = "kevinhwang91/nvim-hlslens" }
  require("hlslens").setup {}
  util.mapt {
    {
      "n",
      "n",
      "<Cmd>execute('normal! ' . v:count1 . 'n')<CR>zv<Cmd>lua require('hlslens').start()<CR>",
    },
    {
      "n",
      "N",
      "<Cmd>execute('normal! ' . v:count1 . 'N')<CR>zv<Cmd>lua require('hlslens').start()<CR>",
    },
    { "n", "*", "*zv<Cmd>lua require('hlslens').start()<CR>" },
    { "n", "#", "#zv<Cmd>lua require('hlslens').start()<CR>" },
    { "n", "g*", "g*zv<Cmd>lua require('hlslens').start()<CR>" },
    { "n", "g#", "g#zv<Cmd>lua require('hlslens').start()<CR>" },
  }
end)

later(function()
  add {
    source = "nvim-telescope/telescope.nvim",
    checkout = "master",
    depends = { "folke/trouble.nvim" },
  }
  add { source = "nvim-telescope/telescope-ui-select.nvim" }
  add {
    source = "nvim-telescope/telescope-fzf-native.nvim",
    hooks = {
      post_install = function(args)
        _G.assert((nil ~= args), "Missing argument args on unknown:162")
        local compile_job = vim.system({ "make" }, { cwd = args.path })
        compile_job:wait()
      end,
    },
  }
  require("telescope").load_extension "fzf"
  require("telescope").load_extension "ui-select"
  require "_.plugins.telescope"
end)

later(function()
  add {
    source = "zk-org/zk-nvim",
    depends = { "nvim-telescope/telescope.nvim" },
  }
  require("telescope").load_extension "zk"
  require "_.plugins.zk"
end)

now(function()
  add {
    source = "L3MON4D3/LuaSnip",
    checkout = "master",
    depends = {
      "rafamadriz/friendly-snippets",
      "honza/vim-snippets",
    },
    hooks = {
      post_install = function(args)
        _G.assert((nil ~= args), "Missing argument args on unknown:187")
        local compile_job = vim.system(
          { "make", "install_jsregexp" },
          { cwd = args.path }
        )
        compile_job:wait()
      end,
    },
  }
  -- add {
  --   source = "https://gitlab.com/repetitivesin/madol.nvim.git",
  --   checkout = "HEAD",
  -- }
  add {
    source = "/home/kibi/Projects/nvim/madol.nvim",
    checkout = "HEAD",
  }
  require "_.plugins.luasnip"
end)

later(function()
  add {
    source = "hrsh7th/nvim-cmp",
    depends = {
      "FelipeLema/cmp-async-path",
      "PaterJason/cmp-conjure",
      "hrsh7th/cmp-buffer",
      "hrsh7th/cmp-cmdline",
      "hrsh7th/cmp-nvim-lsp",
      "hrsh7th/cmp-nvim-lsp-signature-help",
      "hrsh7th/cmp-nvim-lua",
      "hrsh7th/cmp-path",
      "kdheepak/cmp-latex-symbols",
      "L3MON4D3/LuaSnip",
      "saadparwaiz1/cmp_luasnip",
      "onsails/lspkind-nvim",
    },
  }
  require "_.plugins.cmp"
end)

later(function()
  add {
    source = "windwp/nvim-autopairs",
    depends = { "hrsh7th/nvim-cmp" },
  }
  require "_.plugins.autopairs"
end)

later(function()
  add {
    source = "ggandor/leap.nvim",
    depends = { "tpope/vim-repeat" },
  }
  add {
    source = "https://gitlab.com/repetitivesin/flit.nvim.git",
    checkout = "my/repeat-last-motion",
    depends = { "ggandor/leap.nvim" },
  }
  require "_.plugins.leap"
end)

add {
  source = "nvim-neo-tree/neo-tree.nvim",
  checkout = "v3.x",
  depends = {
    "nvim-lua/plenary.nvim",
    "nvim-tree/nvim-web-devicons",
    "MunifTanjim/nui.nvim",
  },
}
require "_.plugins.neo-tree"

later(function()
  add {
    source = "NeogitOrg/neogit",
    depends = {
      "nvim-lua/plenary.nvim",
      "lewis6991/gitsigns.nvim",
      "sindrets/diffview.nvim",
      "nvimtools/hydra.nvim",
    },
  }
  require "_.plugins.git"
end)

add { source = "nvimtools/hydra.nvim" }
require "_.plugins.hydra"

now(function()
  add { source = "gpanders/nvim-parinfer" }
  vim.g.parinfer_no_maps = true
  vim.g.parinfer_force_balance = true
  table.insert(vim.g.parinfer_filetypes, "query")
end)

later(function()
  add {
    source = "3rd/image.nvim",
    depends = { "nvim-treesitter/nvim-treesitter" },
  }

  require("image").setup {
    max_width = 80,
    max_height = 20,
    max_height_window_percentage = math.huge,
    max_width_window_percentage = math.huge,
    window_overlap_clear_enabled = true,
    window_overlap_clear_ft_ignore = { "cmp_menu", "cmp_docs", "", "notify" },
  }
end)

now(function()
  add {
    source = "mrcjkb/haskell-tools.nvim",
    depends = {
      "nvim-lua/plenary.nvim",
      "nvim-telescope/telescope.nvim",
      "neovim/nvim-lspconfig",
    },
    checkout = "master",
  }
  local lsp = require "_.plugins.lsp"
  local on_attach = lsp["ext-config"]({})["on_attach"]
  vim.g.haskell_tools = { hls = { on_attach = on_attach } }
end)

add {
  source = "https://git.sr.ht/~p00f/clangd_extensions.nvim/",
  name = "clangd_extensions",
}
add { source = "elkowar/yuck.vim" }

-- (use :kmonad/kmonad-vim)
-- (use :Julian/lean.nvim
--      {:event ["BufReadPre *.lean" "BufNewFile *.lean"]
--       :config (λ [] (local lean (require :lean))
--                     (local lsp (require :_.plugins.lsp))
--                     (local {: on_attach} (lsp.ext-config {}))
--                     (lean.setup
--                       {:lsp {: on_attach}
--                        :mappings true}))})
--
--
-- (use :quarto-dev/quarto-nvim
--      {:dependencies [:jmbuhr/otter.nvim
--                      :neovim/nvim-lspconfig
--                      :nvim-treesitter/nvim-treesitter
--                      :benlubas/molten-nvim]
--       :config #(cfg :quarto)})