local leap = require "leap"
local flit = require "flit"
local util = require "_.util"

leap.setup { case_sensetive = false }

local function leap_bidirectional()
  return leap.leap { target_windows = { vim.api.nvim_get_current_win() } }
end

local function leap_all_windows()
  local function win_is_focusable(window)
    return vim.api.nvim_win_get_config(window).focusable
  end

  return leap.leap {
    target_windows = vim.tbl_filter(
      win_is_focusable,
      vim.api.nvim_tabpage_list_wins(0)
    ),
  }
end

util.mapt {
  { "x", "s", leap_bidirectional },
  { "o", "x", leap_bidirectional },
  { "n", "s", leap_all_windows },
}

leap.add_repeat_mappings(
  ";",
  ",",
  { relative_direction = true, modes = { "n", "x" } }
)

flit.setup()