local lspconfig = require "lspconfig"
local cmp_lsp = require "cmp_nvim_lsp"
local lsp_lines = require "lsp_lines"
local hydra = require "hydra"
local builtin = require "telescope.builtin"
local util = require "_.util"
require("lazydev").setup {
  library = {
    {
      path = "${3rd}/luv/library",
      words = { "vim%.uv" },
    },
    {
      path = vim.fn.stdpath("config") .. "/lua/luals-annotations",
      mods = { "pandoc" },
    },
  },
}

local border = {
  { "╭", "FloatBorder" },
  { "─", "FloatBorder" },
  { "╮", "FloatBorder" },
  { "│", "FloatBorder" },
  { "╯", "FloatBorder" },
  { "─", "FloatBorder" },
  { "╰", "FloatBorder" },
  { "│", "FloatBorder" },
}

local orig_util_open_floating_preview = vim.lsp.util.open_floating_preview

local function new_open_fl_pr(contents, syntax, _opts, ...)
  local opts = (_opts or {})
  opts["border"] = (opts.border or border)
  return orig_util_open_floating_preview(contents, syntax, opts, ...)
end

vim.lsp.util.open_floating_preview = new_open_fl_pr

local lsp_keymaps = {
  {
    "n",
    "<C-k>",
    vim.lsp.buf.signature_help,
    { desc = "Signature help" },
  },
  { "n", "K", vim.lsp.buf.hover, { desc = "lsp hover" } },
}
local lsp_hydra_hint = [[
888      .d8888b.  8888888b.^^
888     d88P  Y88b 888   Y88b^^ ^ _n_: rename          ^  _d_: line diagnostics  ^
888     Y88b.      888    888^^ ^ _j_: next diagnostics^  _k_: prev diagnostics  ^
888      \"Y888b.   888   d88P^ ^ _f_: format buffer   ^  _l_: toggle lsp-lines  ^
888         \"Y88b. 8888888P\"  ^ _p_: diagnostics     ^  _c_: actions           ^
888           \"888 888^        ^ _e_: definitions     ^  _r_: references        ^
888     Y88b  d88P 888^^        ^ _b_: navbuddy        ^  _s_: workspace symbols ^
88888888 \"Y8888P\"  888        ^ ^ ^                  ^               _<Esc>_   ^]]

local lsp_hydra
local function open_diagnostics()
  return vim.diagnostic.open_float {
    bufnr = 0,
    scope = "line",
  }
end

local function format_async()
  return vim.lsp.buf.format { async = true }
end

lsp_hydra = {
  name = "LSP",
  hint = lsp_hydra_hint,
  mode = "n",
  body = "<Leader>l",
  config = {
    color = "teal",
    invoke_on_body = true,
    hint = { position = "middle", float_opts = { border = "rounded" } },
  },
  heads = {
    { "n", vim.lsp.buf.rename, { desc = "rename symbol" } },
    { "d", open_diagnostics, { desc = "line diagnostics" } },
    {
      "j",
      vim.diagnostic.goto_next,
      { desc = "diagnostics next", exit = false },
    },
    {
      "k",
      vim.diagnostic.goto_prev,
      { desc = "diagnostics prev", exit = false },
    },
    { "f", format_async, { desc = "Format code" } },
    {
      "l",
      lsp_lines.toggle,
      { desc = "Toggle lsp_lines", exit = false },
    },
    { "p", builtin.diagnostics, { desc = "project diagnostics" } },
    { "c", vim.lsp.buf.code_action, { desc = "code actions" } },
    { "e", builtin.lsp_definitions, { desc = "telescope lsp definitions" } },
    { "r", builtin.lsp_references, { desc = "telescope lsp references" } },
    { "b", "<Cmd>Navbuddy<CR>", { desc = "navbuddy" } },
    {
      "s",
      builtin.lsp_workspace_symbols,
      { desc = "telescope LSP workspace symbols" },
    },
    { "<Esc>", nil, { exit = true } },
  },
}

local function on_attach(_client, buffer)
  vim.bo[buffer]["omnifunc"] = "v:lua.vim.lsp.omnifunc"
  util.mapt(lsp_keymaps, { buffer = buffer })
  return hydra(vim.tbl_deep_extend("keep", {
    config = { buffer = buffer },
  }, lsp_hydra))
end

local base_config = {
  on_attach = on_attach,
  capabilities = vim.tbl_deep_extend("force", cmp_lsp.default_capabilities(), {
    textDocument = {
      foldingRange = { lineFoldingOnly = true, dynamicRegistration = false },
    },
  }),
}

local function ext_config(...)
  return vim.tbl_extend("force", base_config, ...)
end

local servers_configuration = {
  nixd = {},
  digestif = { filetypes = { "context" } },
  texlab = { filetypes = { "bib", "tex" } },
  -- hls = {}, -- configured by haskell-tools.nvim
  jqls = {},
  clangd = { cmd = { "clangd", "--clang-tidy", "--fallback-style=google" } },
  -- broken as of 2024-10-27 due to go-lsp not supporting semantic tokens
  arduino_language_server = {},
  fennel_ls = {
    settings = {
      fennel = {
        workspace = { library = vim.api.nvim_list_runtime_paths() },
        diagnostics = { globals = { "fennel", "vim", "util" } },
      },
    },
  },
  lua_ls = {},
  neocmake = {},
  ansiblels = {},
  basedpyright = {},
  -- marksman = {},
}

local function setup_language_servers()
  for server_name, v in pairs(servers_configuration) do
    local function get_config()
      local conf_type = type(v)
      if conf_type == "function" then
        return v()
      elseif conf_type == "table" then
        return v
      else
        return {}
      end
    end

    lspconfig[server_name].setup(ext_config(get_config()))
  end
end

require("rust-tools").setup { server = ext_config {} }
setup_language_servers()

return { ["ext-config"] = ext_config }