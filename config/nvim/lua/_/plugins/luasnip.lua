local ls = require "luasnip"
local madol = require "madol"
local lsf = require "luasnip.extras.fmt"
local c = ls.choice_node
local i = ls.insert_node
local t = ls.text_node
local sn = ls.snippet_node
local r = ls.restore_node

ls.config.setup {
  history = true,
  update_events = { "TextChanged", "TextChangedI" },
  delete_check_events = { "TextChanged" },
  enable_autosnippets = true,
  store_selection_keys = "<Tab>",
}

require("luasnip.loaders.from_vscode").lazy_load()
require("luasnip.loaders.from_snipmate").lazy_load()

local function notes(is_math, is_not_math)
  local snippets = {}

  local sst = ls.extend_decorator.apply(
    ls.snippet,
    { condition = is_not_math, snippetType = "autosnippet" }
  )

  local function st(...)
    return table.insert(snippets, sst(...))
  end

  local ssm = ls.extend_decorator.apply(
    ls.snippet,
    { condition = is_math, snippetType = "autosnippet" }
  )

  local function sm(...)
    return table.insert(snippets, ssm(...))
  end

  st(
    {
      trig = "hv--",
      name = "Idea",
    },
    lsf.fmta("### <title>\n\n<contents>\n", {
      title = i(1, "IDEA"),
      contents = i(2, "IDEA CONTENTS"),
    })
  )

  sm(
    {
      trig = "zpn",
      name = "Zbieżność prawie na pewno",
    },
    lsf.fmta("\\overset{<ch_typ>}{<ch_cont>} ", {
      ch_typ = c(1, { t "\\text{p.n.}", t "\\text{p}", t "L^p" }),
      ch_cont = c(2, {
        t "\\longrightarrow",
        sn(
          nil,
          lsf.fmta("\\underset{<>}{\\longrightarrow}", { r(1, "restore-text") })
        ),
      }),
    }),
    { stored = { ["restore-text"] = i(1, "n\\to\\infty") } }
  )

  st(
    {
      trig = "thm!",
      name = "theorem fenced div",
    },
    lsf.fmta(
      "::: {.theorem title=\"<title>\" ref=\"<label>\"}\n<contents>\n:::",
      {
        title = i(1, ""),
        label = i(2, ""),
        contents = i(3, ""),
      }
    )
  )

  st(
    {
      trig = "lem!",
      name = "lemma fenced div",
    },
    lsf.fmta(
      "::: {.lemma title=\"<title>\" ref=\"<label>\"}\n<contents>\n:::",
      {
        title = i(1, ""),
        label = i(2, ""),
        contents = i(3, ""),
      }
    )
  )

  st(
    {
      trig = "ex!",
      name = "example fenced div",
    },
    lsf.fmta(
      "::: {.example title=\"<title>\" ref=\"<label>\"}\n<contents>\n:::",
      {
        title = i(1, ""),
        label = i(2, ""),
        contents = i(3, ""),
      }
    )
  )

  st(
    {
      trig = "ca!",
      name = "caution fenced div",
    },
    lsf.fmta(
      "::: {.caution title=\"<title>\" ref=\"<label>\"}\n<contents>\n:::",
      {
        title = i(1, ""),
        label = i(2, ""),
        contents = i(3, ""),
      }
    )
  )

  return snippets
end

madol.setup {
  markdown = {
    enable = true,
    snippets = {
      [notes] = true,
      ["greek-tex"] = true,
      ["greek-unicode"] = false,
    },
  },
}