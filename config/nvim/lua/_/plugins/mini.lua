local ai = require "mini.ai"
local align = require "mini.align"
local bufremove = require "mini.bufremove"
local clue = require "mini.clue"
local comment = require "mini.comment"
local cursorword = require "mini.cursorword"
local hipatterns = require "mini.hipatterns"
local statusline = require "mini.statusline"
local move = require "mini.move"
local session = require "mini.sessions"
local starter = require "mini.starter"
local surround = require "mini.surround"
local tabline = require "mini.tabline"
local util = require "_.util"

do
  local function whole_buffer_tobj()
    return {
      from = { line = 1, col = 1 },
      to = {
        line = vim.fn.line "$",
        col = math.max(vim.fn.getline("$"):len(), 1),
      },
    }
  end

  ai.setup {
    n_lines = 100000,
    custom_textobjects = { g = whole_buffer_tobj },
    search_method = "cover",
  }

  vim.api.nvim_create_autocmd("BufEnter", {
    pattern = { "*.md", "*.tex", "*.qmd", "*.rmd" },
    callback = function()
      vim.b.miniai_config = {
        custom_textobjects = {
          m = { "[^$]()$().-()$()[^$]" },
          d = { "$$\n%s+().-()\n$%$", "$$().-()$%$" },
        },
      }
    end,
  })
end

do
  local star_sur = {
    input = function()
      local n_star = surround.user_input "Number of * to find"
      local many_star = string.rep("%*", (tonumber(n_star) or 1))
      return { (many_star .. "().-()" .. many_star) }
    end,
    output = function()
      local n_star = surround.user_input "Number of * to output"
      local many_star = string.rep("*", (tonumber(n_star) or 1))
      return { left = many_star, right = many_star }
    end,
  }

  local inline_math_sur = {
    output = { left = "$", right = "$" },
    input = { "[^$]()$().-()$()[^$]" },
  }

  local display_math_sur = {
    output = { left = "$$\n", right = "\n$$" },
    input = { { "$$\n%s+().-()\n%s+$%$", "$$().-()$%$" } },
  }

  local code_block_sur = {
    output = { left = "```\n", right = "\n```" },
    input = {
      {
        "```\n%s+().-()\n%s+```",
        "``` ?%S+\n().-()\n%s+```",
        "``` ?{.-}\n().-()\n%s+```",
      },
    },
  }

  vim.api.nvim_create_autocmd("BufEnter", {
    pattern = { "*.md", "*.tex", "*.qmd", "*.rmd" },
    callback = function()
      vim.b.minisurround_config = {
        custom_surroundings = {
          ["*"] = star_sur,
          m = inline_math_sur,
          d = display_math_sur,
          t = code_block_sur,
        },
      }
    end,
  })

  surround.setup {
    mappings = {
      add = "ys",
      delete = "ds",
      find = "",
      find_left = "",
      highlight = "",
      replace = "cs",
      suffix_last = "",
      suffix_next = "",
      update_n_lines = "",
    },
    custom_surroundings = {
      ["*"] = star_sur,
      m = inline_math_sur,
      d = display_math_sur,
    },
    search_method = "cover",
  }
  vim.keymap.del("x", "ys")
  vim.keymap.set(
    "x",
    "S",
    ":<C-u>lua MiniSurround.add('visual')<CR>",
    { silent = true }
  )
  vim.keymap.set("n", "yss", "ys_", { remap = true })
end

align.setup { mappings = { start = "gA", start_with_preview = "ga" } }
cursorword.setup {}

clue.setup {
  clues = {
    clue.gen_clues.builtin_completion(),
    clue.gen_clues.g(),
    clue.gen_clues.marks(),
    clue.gen_clues.registers(),
    clue.gen_clues.windows(),
    clue.gen_clues.z(),
  },
  triggers = {
    { keys = "<Leader>", mode = "n" },
    { keys = "<Leader>", mode = "x" },
    { keys = "<C-x>", mode = "i" },
    { keys = "g", mode = "n" },
    { keys = "g", mode = "x" },
    { keys = "'", mode = "n" },
    { keys = "`", mode = "n" },
    { keys = "'", mode = "x" },
    { keys = "`", mode = "x" },
    { keys = "\"", mode = "n" },
    { keys = "\"", mode = "x" },
    { keys = "<C-r>", mode = "i" },
    { keys = "<C-r>", mode = "c" },
    { keys = "<C-w>", mode = "n" },
    { keys = "z", mode = "n" },
    { keys = "z", mode = "x" },
  },
  window = { delay = 200 },
}

hipatterns.setup {
  highlighters = {
    fixme = {
      group = "MiniHipatternsFixme",
      pattern = "%f[%w]()FIXME()%f[%w]",
    },
    hack = { group = "MiniHipatternsHack", pattern = "%f[%w]()HACK()%f[%W]" },
    note = { group = "MiniHipatternsNote", pattern = "%f[%w]()NOTE()%f[%W]" },
    todo = { group = "MiniHipatternsTodo", pattern = "%f[%w]()TODO()%f[%W]" },
    hex_color = hipatterns.gen_highlighter.hex_color(),
  },
}

move.setup()

session.setup {
  autowrite = true,
  verbose = { read = true, write = true, delete = true },
  autoread = false,
}

comment.setup()
starter.setup()
tabline.setup()
statusline.setup()

bufremove.setup { set_vim_settings = true }
util.mapt {
  { "n", "<Leader>bd", bufremove.delete, { desc = "Delete buffer" } },
  { "n", "<Leader>bh", bufremove.unshow, { desc = "Hide buffer" } },
}

local trailspace = require "mini.trailspace"
local au_buf = vim.api.nvim_create_augroup("buf-trim", { clear = true })
return vim.api.nvim_create_autocmd("BufWritePre", {
  pattern = "*",
  group = au_buf,
  callback = function(_args)
    return trailspace.trim()
  end,
})