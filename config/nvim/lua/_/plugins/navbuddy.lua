local navbuddy = require "nvim-navbuddy"

return navbuddy.setup {
  lsp = {
    auto_attach = true,
    preference = { "hls", "lua_ls" },
  },
}