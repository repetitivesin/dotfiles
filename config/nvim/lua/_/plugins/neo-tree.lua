local util = require "_.util"
local neo_tree = require "neo-tree"

neo_tree.setup {}

return util.map("n", "<C-n>", "<Cmd>Neotree toggle position=right<CR>")