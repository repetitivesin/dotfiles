local nl = require "null-ls"

local lsp_conf = require("_.plugins.lsp")["ext-config"] {}

local on_attach = lsp_conf["on_attach"]

local sources = {
  ---- Formatters
  nl.builtins.formatting.fnlfmt,
  nl.builtins.formatting.nixpkgs_fmt,
  -- nl.builtins.formatting.mdformat
  nl.builtins.formatting.prettier,
  nl.builtins.formatting.shfmt,
  nl.builtins.formatting.stylua,
  nl.builtins.formatting.fish_indent,
  require "none-ls.formatting.ruff",

  ---- Completion
  -- nl.builtins.completion.spell

  ---- Diagnostics
  nl.builtins.diagnostics.statix,
  nl.builtins.diagnostics.fish,
  require "none-ls.diagnostics.ruff",

  ---- Code actions
  nl.builtins.code_actions.refactoring,
  nl.builtins.code_actions.statix,
}

nl.setup {
  sources = sources,
  on_attach = on_attach,
}