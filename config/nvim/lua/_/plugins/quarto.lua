local quarto = require "quarto"

quarto.setup {
  closePreviewOnExit = true,
  codeRunner = { enabled = false },
  lspFeatures = {
    chunks = "curly",
    completion = { enabled = true },
    diagnostics = { enabled = true, triggers = { "BufWritePost" } },
    enabled = true,
    languages = { "r", "cpp", "python", "julia", "bash", "html" },
  },
}