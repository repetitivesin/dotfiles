local telescope = require "telescope"
local hydra = require "hydra"
local builtin = require "telescope.builtin"
local themes = require "telescope.themes"
local actions = require "telescope.actions"
local trouble_tp = require "trouble.sources.telescope"
local util = require "_.util"

telescope.setup {
  defaults = {
    prompt_prefix = "   ",
    selection_caret = "  ",
    entry_prefix = "  ",
    initial_mode = "insert",
    selection_strategy = "reset",
    sorting_strategy = "ascending",
    scroll_strategy = "limit",
    layout_strategy = "horizontal",
    layout_config = {
      horizontal = {
        prompt_position = "top",
        preview_width = 0.55,
        results_width = 0.8,
      },
      vertical = { mirror = false },
      width = 0.87,
      height = 0.8,
      preview_cutoff = 120,
    },
    preview = {
      filesize_limit = 10,
      timeout = 500,
      treesitter = true,
      msg_bg_fillchar = "#",
    },
    file_ignore_patterns = { "node_modules", "lua/fun.lua" },
    path_display = { "absolute" },
    winblend = 0,
    border = true,
    color_devicons = true,
    mappings = {
      i = {
        ["<C-j>"] = actions.move_selection_next,
        ["<C-k>"] = actions.move_selection_previous,
        ["<C-s>"] = actions.select_horizontal,
        ["<C-t>"] = trouble_tp.open,
      },
      n = {
        ["<C-s>"] = actions.select_horizontal,
        ["<C-t>"] = trouble_tp.open,
      },
    },
  },
  pickers = {
    resume = { initial_mode = "normal" },
    man_pages = { sections = { "ALL" } },
    find_files = { find_command = { "fd", "-tf", "-L" } },
  },
  extensions = {
    fzf = {
      fuzzy = true,
      override_generic_sorter = true,
      override_file_sorter = true,
      case_mode = "ignore_case",
    },
    ["ui-select"] = { themes.get_dropdown {} },
  },
}

local telescope_hint = [[
^   🭇🬭🬭🬭🬭🬭🬭🬭🬭🬼    _f_: files          _o_: old files
^  🭉🭁🭠🭘    🭣🭕🭌🬾   _._: search in file _g_: rg
^  🭅█ ▁     █🭐
^  ██🬿      🭊██
^ 🭋█🬝🮄🮄🮄🮄🮄🮄🮄🮄🬆█🭀  _a_: vim options    _h_: vim help
^ 🭤🭒🬺🬹🬱🬭🬭🬭🬭🬵🬹🬹🭝🭙  _i_: notifications  _m_: manpages
^                 _k_: keymaps        _j_: highlight groups ^
^                 _t_: telescope      _<Esc>_: exit
^]]

local telescope_hydra = {
  name = "Telescope",
  hint = telescope_hint,
  mode = "n",
  body = "<Leader>f",
  config = {
    color = "teal",
    invoke_on_body = true,
    hint = { position = "middle", float_opts = { border = "rounded" } },
  },
  heads = {
    { "f", builtin.find_files, { desc = "find files" } },
    { "g", builtin.live_grep,  { desc = "live grep" } },
    {
      "o",
      builtin.oldfiles,
      { desc = "recently opened files" },
    },
    { "h", builtin.help_tags,   { desc = "vim help" } },
    { "m", builtin.man_pages,   { desc = "marks" } },
    { "k", builtin.keymaps,     { desc = "keymaps" } },
    { "a", builtin.vim_options, { desc = "vim options" } },
    {
      ".",
      builtin.current_buffer_fuzzy_find,
      { desc = "search in file" },
    },
    { "t",     "<Cmd>Telescope<CR>", { desc = "All pickers" } },
    {
      "i",
      telescope.extensions.notify.notify,
      { desc = "notifications sent with vim.notify" },
    },
    {
      "j",
      builtin.highlights,
      { desc = "highlight groups" },
    },
    { "<Esc>", nil,                  { exit = true } },
  },
}
hydra(telescope_hydra)
return util.mapt({
  { "n", "<A-Tab>", builtin.buffers, { desc = "telescope buffers" } },
  { "n", "<A-r>",   builtin.resume,  { desc = "resume telescope" } },
}, { silent = true })