local ts_conf = require "nvim-treesitter.configs"

return ts_conf.setup {
  ensure_installed = {
    "lua",
    "vim",
    "fennel",
    "markdown",
    "markdown_inline",
    "bash",
    "fish",
    "nix",
    "html",
    "latex",
  },
  highlight = { enable = true, additional_vim_regex_hightlighting = { "org" } },
  incremental_selection = { enable = false },
  indent = { enable = true },
  playground = { enable = true },
}