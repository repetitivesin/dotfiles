local ufo = require "ufo"
local util = require "_.util"
local hydra = require "hydra"

vim.api.nvim_create_autocmd("BufReadPre", {
  callback = function()
    vim.b.ufo_foldlevel = 5
  end,
})

local function set_buf_foldlevel(num)
  vim.b.ufo_foldlevel = num
  ufo.closeFoldsWith(num)
end

local function change_buf_foldlevel_by(num)
  local foldlevel = vim.b.ufo_foldlevel or 0
  if foldlevel + num >= 0 then
    foldlevel = foldlevel + num
  else
    foldlevel = 0
  end
  set_buf_foldlevel(foldlevel)
end

local function fold_more()
  local count = vim.v.count
  if count == 0 then
    count = 1
  end
  change_buf_foldlevel_by(-count)
end

local function fold_less()
  local count = vim.v.count
  if count == 0 then
    count = 1
  end
  change_buf_foldlevel_by(count)
end

ufo.setup {
  provider_selector = function()
    return { "treesitter", "indent" }
  end,
}

local fold_hint = [[
  _m_: fold more   _c_: close      _a_: toggle
  _M_: fold all    _C_: close all  _A_: toggle all
  _r_: fold less   _o_: open       ^^ Fold level
  _R_: unfold all  _O_: open all       %{fdl}
]]

local fold_hydra = {
  name = "Fold",
  mode = "n",
  body = "z",
  hint = fold_hint,
  config = {
    color = "pink",
    invoke_on_body = false,
    hint = {
      type = "window",
      position = "bottom",
      float_opts = { border = "rounded" },
      funcs = {
        fdl = function()
          return tostring(vim.b.ufo_foldlevel or vim.opt.foldlevel:get())
        end,
      },
    },
  },
  heads = {
    { "m", fold_more },
    { "M", ufo.closeAllFolds },
    { "r", fold_less },
    { "R", ufo.openAllFolds },
    { "c", "zc" },
    { "C", "zC" },
    { "o", "zo" },
    { "O", "zO" },
    { "a", "za" },
    { "A", "zA" },
  },
}

hydra(fold_hydra)