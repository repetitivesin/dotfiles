local zk = require "zk"
local lsp = require "_.plugins.lsp"
local telescope = require "telescope"
local builtin = require "telescope.builtin"
local hydra = require "hydra"

zk.setup {
  picker = "telescope",
  lsp = { config = lsp["ext-config"] {} },
}
telescope.load_extension "zk"

local zk_hint = [[

                  _n_: new note        _p_: insert link
███████╗██╗  ██╗  _o_: list orphans
╚══███╔╝██║ ██╔╝  _s_: list notes      _t_: list tags
  ███╔╝ █████╔╝   _i_: list links      _b_: list backlinks
 ███╔╝  ██╔═██╗   _g_: rg hidden       _f_: fd hidden
███████╗██║  ██╗  _h_: cd $ZK          ^ ^
╚══════╝╚═╝  ╚═╝  ^ ^                  ^ ^
                  ^ ^                              _<Esc>_]]
local function insert_link()
  local cur_mode = vim.api.nvim_get_mode().mode

  if cur_mode == "v" or cur_mode == "V" then
    return ":ZkInsertLinkAtSelection<CR>"
  elseif cur_mode == "n" then
    return "<Cmd>ZkInsertLink<CR>"
  else
    vim.notify(
      "Not in appropriate mode for link insertion(expected normal, visual, visual block)",
      vim.log.levels.ERROR
    )
    return ""
  end
end

local function zk_new()
  return zk.new()
end

local function zk_list()
  return zk.edit()
end

local function zk_tags()
  return telescope.extensions.zk.tags()
end

local function fd_hidden()
  return builtin.find_files { hidden = true, no_ignore = true }
end

local function rg_hidden()
  return builtin.live_grep { hidden = true, no_ignore = true }
end

local function zk_list_orphans()
  return zk.edit { orphan = true }
end

local function zk_cd()
  return zk.cd()
end

return hydra {
  name = "Zettelkasten",
  hint = zk_hint,
  mode = { "n", "v" },
  body = "<Leader>n",
  config = {
    color = "teal",
    invoke_on_body = true,
    hint = { position = "middle", float_opts = { border = "rounded" } },
  },
  heads = {
    { "n", zk_new, { desc = "New note" } },
    { "p", insert_link, { desc = "Insert link", expr = true } },
    { "s", zk_list, { desc = "List notes" } },
    { "t", zk_tags, { desc = "List tags" } },
    { "f", fd_hidden, { desc = "Find files" } },
    { "g", rg_hidden, { desc = "Grep files" } },
    { "i", "<Cmd>ZkLinks<CR>", { desc = "List links" } },
    { "b", "<Cmd>ZkBacklinks<CR>", { desc = "List backlinks" } },
    { "o", zk_list_orphans, { desc = "List orphans" } },
    { "h", zk_cd, { desc = "cd $ZK", exit = false } },
    { "<Esc>", nil, { exit = true, nowait = true } },
  },
}