local function mapt(t, _3foverride_opt)
  local override_opt = (_3foverride_opt or {})
  for _, v in ipairs(t) do
    local mode = v[1]
    local lhs = v[2]
    local rhs = v[3]
    local opts = v[4]
    local opts0 = vim.tbl_extend("keep", override_opt, (opts or {}))
    vim.keymap.set(mode, lhs, rhs, opts0)
  end
end

local function map(mode, lhs, rhs, opts_3f)
  return vim.keymap.set(mode, lhs, rhs, (opts_3f or {}))
end

local function reqcall(module_name, field, ...)
  return require(module_name)[field](...)
end

local function reqdot(module_name, ...)
  return require(module_name)[...]
end

local function rt(key)
  return vim.api.nvim_replace_termcodes(key, true, true, true)
end

local function fk(key, _3fmode)
  return vim.api.nvim_feedkeys(rt(key), (_3fmode or ""), true)
end

return {
  mapt = mapt,
  map = map,
  reqcall = reqcall,
  reqdot = reqdot,
  rt = rt,
  fk = fk,
}