{
  description = "Test nixos flake configuration";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-24.11";
    nixpkgs2405.url = "github:nixos/nixpkgs/nixos-24.05";
    nixpkgs2311.url = "github:nixos/nixpkgs/nixos-23.11";
    nixpkgs-unstable.url = "github:nixos/nixpkgs/nixos-unstable";
    home-manager = {
      url = "github:nix-community/home-manager/release-24.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixos-hardware.url = "github:NixOS/nixos-hardware/master";
    sops-nix = {
      url = "github:Mic92/sops-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    # neovim-nightly.url = "github:nix-community/neovim-nightly-overlay";
    # neovim-nightly.inputs.nixpkgs.follows = "nixpkgs-unstable";
    lanzaboote = {
      url = "github:nix-community/lanzaboote/v0.4.1";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    codeblocks-nix = {
      url = "gitlab:repetitivesin/codeblocks-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs =
    { self
    , nixpkgs
    , nixpkgs-unstable
    , nixpkgs2405
    , nixpkgs2311
    , home-manager
    , nixos-hardware
    , ...
    }@inputs: {

      nixosConfigurations = {
        "thinkcentre-alpha" =
          let
            system = "x86_64-linux";
            overlays = [ ];
            nixpkgs-config = {
              allowUnfree = true;
            };
            pkgs = import nixpkgs {
              inherit system overlays;
              config = nixpkgs-config;
            };
          in
          nixpkgs.lib.nixosSystem {
            inherit system pkgs;
            specialArgs = { inherit inputs; };
            modules = [
              { system.stateVersion = "24.05"; }
              inputs.sops-nix.nixosModules.sops
              {
                sops = {
                  defaultSopsFile = ./secrets/thinkcentre-alpha.yaml;
                  age.keyFile = "/home/tcs/.config/sops/age/keys.txt";
                };
              }
              "${home-manager}/nixos"
              ./nix/sys/alpha
            ];
          };
        t14s =
          let
            system = "x86_64-linux";
            pkgs = import nixpkgs { inherit system overlays; config.allowUnfree = true; };
            overlays = [
              (final: prev: {
                unstable = import nixpkgs-unstable
                  { inherit system overlays; config.allowUnfree = true; };
                nixpkgs2311 = import nixpkgs2311
                  { inherit system overlays; config.allowUnfree = true; };
                nixpkgs2405 = import nixpkgs2405
                  { inherit system overlays; config.allowUnfree = true; };
                home-manager = home-manager.defaultPackage.${system};
              })
            ];
          in
          nixpkgs.lib.nixosSystem {
            inherit system pkgs;
            specialArgs = { inherit inputs; };
            modules = [
              inputs.lanzaboote.nixosModules.lanzaboote
              { system.stateVersion = "24.05"; }
              "${home-manager}/nixos"
              inputs.sops-nix.nixosModules.sops
              {
                sops = {
                  defaultSopsFile = ./secrets/thinkcentre-alpha.yaml;
                  age.keyFile = "/home/kibi/.config/sops/age/keys.txt";
                };
              }
              nixos-hardware.nixosModules.lenovo-thinkpad-t14s-amd-gen1
              ./nix/sys/t14s
            ];
          };

        yoga =
          let
            system = "x86_64-linux";
            pkgs = import nixpkgs { inherit system overlays; config.allowUnfree = true; };
            overlays = [
              (final: prev: {
                unstable = import nixpkgs-unstable
                  { inherit system overlays; config.allowUnfree = true; };
                nixpkgs2311 = import nixpkgs2311
                  { inherit system overlays; config.allowUnfree = true; };
                nixpkgs2405 = import nixpkgs2405
                  { inherit system overlays; config.allowUnfree = true; };
                home-manager = home-manager.defaultPackage.${system};
              })
            ];
          in
          nixpkgs.lib.nixosSystem {
            inherit system pkgs;
            specialArgs = { inherit inputs; };
            modules = [
              inputs.lanzaboote.nixosModules.lanzaboote
              { system.stateVersion = "24.11"; }
              "${home-manager}/nixos"
              inputs.sops-nix.nixosModules.sops
              {
                sops = {
                  defaultSopsFile = ./secrets/thinkcentre-alpha.yaml;
                  age.keyFile = "/home/kibi/.config/sops/age/keys.txt";
                };
              }
              nixos-hardware.nixosModules.lenovo-thinkpad-x1-yoga-7th-gen
              ./nix/sys/yoga
            ];
          };
      };

      homeConfigurations = {
        "kibi@yoga" =
          let
            system = "x86_64-linux";
            pkgs = import nixpkgs
              { inherit system overlays; config.allowUnfree = true; };
            overlays = [
              # inputs.neovim-nightly.overlays.default
              (final: prev: {
                unstable = import nixpkgs-unstable
                  { inherit system overlays; config.allowUnfree = true; };
                nixpkgs2311 = import nixpkgs2311
                  { inherit system overlays; config.allowUnfree = true; };
                nixpkgs2405 = import nixpkgs2405
                  { inherit system overlays; config.allowUnfree = true; };
                stable = import nixpkgs
                  { inherit system overlays; config.allowUnfree = true; };
              })
              (final: prev: {
                codeblocksMost = inputs.codeblocks-nix.packages."${system}".default;
              })
            ];
          in
          home-manager.lib.homeManagerConfiguration {
            inherit pkgs;
            extraSpecialArgs = { inherit inputs; };
            modules = [
              ./nix/hm/kibi
              ./nix/functions.nix
              {
                home = {
                  username = "kibi";
                  stateVersion = "24.11";
                  homeDirectory = "/home/kibi";
                };
              }
            ];
          };
        "kibi@t14s" =
          let
            system = "x86_64-linux";
            pkgs = import nixpkgs
              { inherit system overlays; config.allowUnfree = true; };
            overlays = [
              # inputs.neovim-nightly.overlays.default
              (final: prev: {
                unstable = import nixpkgs-unstable
                  { inherit system overlays; config.allowUnfree = true; };
                nixpkgs2311 = import nixpkgs2311
                  { inherit system overlays; config.allowUnfree = true; };
                nixpkgs2405 = import nixpkgs2405
                  { inherit system overlays; config.allowUnfree = true; };
                stable = import nixpkgs
                  { inherit system overlays; config.allowUnfree = true; };
              })
              (final: prev: {
                codeblocksMost = inputs.codeblocks-nix.packages."${system}".default;
              })
            ];
          in
          home-manager.lib.homeManagerConfiguration {
            inherit pkgs;
            extraSpecialArgs = { inherit inputs; };
            modules = [
              ./nix/hm/kibi
              ./nix/functions.nix
              {
                home = {
                  username = "kibi";
                  stateVersion = "24.05";
                  homeDirectory = "/home/kibi";
                };
              }
            ];
          };
      };

    };
}
