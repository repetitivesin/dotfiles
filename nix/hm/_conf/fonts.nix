{ pkgs, ... }:

{
  home.packages = with pkgs; [
    font-manager
    norwester-font
    encode-sans
    open-sans
    (nerdfonts.override { fonts = [ "JetBrainsMono" "Iosevka" "FiraCode" ]; })
  ];

  fonts.fontconfig.enable = true;
}
