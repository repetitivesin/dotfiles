{ pkgs, ... }:

{
  home.packages = with pkgs; [
    swayidle
    swaylock
    swaybg
    grim
    wev
    libsForQt5.breeze-gtk
    mako
    qt5.qtwayland
    rofi-wayland
    slurp
    waybar
    wl-clipboard
  ];

  wayland.windowManager.hyprland = {
    enable = true;
    systemdIntegration = true;
    extraConfig = null;
  };

}
