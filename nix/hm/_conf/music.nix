{ config, pkgs, lib, ... }:

{
  xdg.userDirs.music = "${config.home.homeDirectory}/Music";

  # example configuration file
  # https://github.com/MusicPlayerDaemon/MPD/blob/master/doc/mpdconf.example
  services.mpd = {
    enable = lib.mkDefault true;
    musicDirectory = config.xdg.userDirs.music;
    network = {
      listenAddress = "localhost";
      port = 6600;
    };
    extraConfig = ''
      restore_paused "yes"
      auto_update "yes"
      follow_outside_symlinks "yes"
      follow_inside_symlinks "yes"
      audio_output {
        type "pipewire"
        name "PW-MPD-Device"
      }
      # filesystem_charset "UTF-8"
    '';
  };

  services.mpdris2.enable = lib.mkDefault true;
  services.mpris-proxy.enable = lib.mkDefault true;

  home.packages = with pkgs; [
    mpc-cli
    puddletag # for mp3 and alike tag editing
    python3Packages.deemix
    spek
    flacon # .cue splitter
    cantata
    easyeffects
    qpwgraph
  ];
}
