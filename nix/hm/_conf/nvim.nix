{ pkgs, lib, ... }:

{
  programs.neovim = {
    enable = true;
    package = pkgs.neovim-unwrapped;
    extraLuaPackages = ps: with ps; [ magick ];
    extraPackages = [ pkgs.imagemagick ];
  };

  home.packages = with pkgs; [
    (pkgs.patchDesktop neovim "nvim"
      [
        "Exec=nvim %F"
        "Terminal=true"
      ]
      [
        "Exec=kitty -e nvim %F"
        "Terminal=false"
      ])

    # UTILITY
    fd
    ripgrep

    # LSP
    unstable.nixd
    # rust-analyzer
    sumneko-lua-language-server
    texlab
    llvmPackages_latest.clang-tools
    cmake-language-server
    marksman
    nodejs_latest
    basedpyright
    unstable.fennel-ls

    # Formatters
    # black
    ruff
    fnlfmt
    jq
    nixpkgs-fmt
    shfmt
    stylua
    nodePackages.prettier

    # Linters
    shellcheck
    statix

    # Required tools
    git
    wget
    tree-sitter

    # DEPENDENCIES(languages)
    gcc
    go
  ];

  home.sessionVariables = rec {
    # PAGER = "nvim";
    MANPAGER = "nvim +Man!";
    EDITOR = "nvim";
    VISUAL = EDITOR;
  };
}
