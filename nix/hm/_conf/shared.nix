{ pkgs, config, ... }:

{
  nixpkgs.overlays = [
    (self: super: {
      mpv = super.mpv.override {
        scripts = with self.mpvScripts; [
          mpris
          webtorrent-mpv-hook
          thumbfast
          uosc
        ];
      };
    })
  ];
  home.packages = with pkgs; [
    # Development
    (python3.withPackages (ps: with ps; [
      dbus-python
      ipython
      sympy
      jc
      ipykernel
    ]))
    gcc
    gnumake
    cmake
    elan
    (rstudioWrapper.override {
      packages = with rPackages; [
        DT
        countrycode
        factoextra
        fst
        gapminder
        hexView
        knitr
        languageserver
        leaflet
        pzfx
        qs
        quarto
        readODS
        rgeoboundaries
        rio
        rmarkdown
        rnaturalearth
        rnaturalearthdata
        s2
        scales
        sf
        tidyverse
        ggcorrplot
        ggridges
        ggrepel
        ggnetwork
        vcd
        ggmosaic
        networkD3
        ggraph
        tidygraph
        hrbrthemes
        fastDummies
        tmap
        udunits2
        viridis
        PerformanceAnalytics
        (igraph.overrideAttrs (prev: {
          buildInputs = prev.buildInputs ++ [ pkgs.libxml2 ];
        }))
      ];
    })
    octaveFull

    # CLIs
    moreutils # parallel && sponge
    file
    glxinfo
    (inxi.override { withRecommends = true; })
    lazygit
    nix-prefetch-git
    p7zip
    tree
    wget
    jq
    yj
    fd
    entr
    zk
    axel
    sqlite
    sad
    delta
    just

    # TUIs
    lf
    (pkgs.patchDesktop lf "lf"
      [
        "Exec=lf"
        "Terminal=true"
        "Categories=ConsoleOnly;System;FileTools;FileManager"
      ]
      [
        "Exec=kitty -e lf"
        "Terminal=false"
        "Categories=System;FileTools;FileManager"
      ])
    orpie
    pipe-rename
    visidata
    htop

    # Benchmarking, battery life, undervolting
    s-tui
    powertop
    # stress-ng
    # sysbench

    # Terminals
    kitty

    # (web)GUIs
    keepassxc
    librewolf
    ventoy
    anki
    antimicrox
    tdesktop
    nomacs
    xournalpp
    rnote

    # Media
    ctpv
    gpick
    yt-dlp
    ffmpeg-full
    mpv
    gimp
    inkscape
    jxrlib
    imagemagickBig

    # Office
    unstable.pandoc_3_6
    mermaid-filter # mermaid diagram filter for pandoc
    librsvg # allow .svg use in pandoc when converting .md -> .pdf
    texlive.combined.scheme-full
    onlyoffice-bin
    nixpkgs2311.zathura
    ghostscript
    poppler_utils
    libreoffice
    pdfarranger
    # scribus
    # pdfarranger
    # geogebra6
  ];

  programs.chromium = {
    enable = true;
    package = pkgs.ungoogled-chromium;
    commandLineArgs = [ "--ozone-platform-hint=wayland" ];
  };

  xdg = {
    enable = true;
    userDirs = {
      enable = true;
      createDirectories = true;
    };
  };

  home.sessionPath = [
    "${config.home.homeDirectory}/.local/bin"
    "${config.home.homeDirectory}/.cargo/bin"
  ];
  programs.man.generateCaches = true;
  programs.git = {
    enable = true;
    userName = "Repetitive";
    userEmail = "git@crii.xyz";
    extraConfig = {
      init = {
        defaultBranch = "main";
      };
      core.pager = "${pkgs.delta}/bin/delta";
      interactive.diffFilter = "${pkgs.delta}/bin/delta --color-only";
      merge.conflictstyle = "diff3";
      diff.colorMoved = "default";
    };
    ignores = [
      "target/"
    ];
  };

  programs.fzf = {
    enable = true;
    enableFishIntegration = false;
  };

  programs.direnv = {
    enable = true;
    nix-direnv.enable = true;
  };
  programs.home-manager.enable = true;
  systemd.user.startServices = "sd-switch";
}
