{ pkgs, config, ... }:

let
  dotfiles =
    config.lib.file.mkOutOfStoreSymlink "${config.home.homeDirectory}/dotfiles";
in
{
  wayland.windowManager.sway = {
    enable = true;
    config = null;
    extraConfig = /*i3config*/''
      include 01config
    '';
    wrapperFeatures = {
      gtk = true;
      base = true;
    };
  };

  services.swayidle = {
    enable = false;
    events = [
      { event = "before-sleep"; command = "${pkgs.swaylock}/bin/swaylock -f"; }
      { event = "lock"; command = "${pkgs.keepassxc}/bin/keepassxc --lock"; }
    ];
    timeouts = [
      { timeout = 300; command = "${pkgs.swaylock}/bin/swaylock -f"; }
      {
        timeout = 600;
        command = "${pkgs.sway}/bin/swaymsg 'output * dpms off'";
        resumeCommand = "${pkgs.sway}/bin/swaymsg 'output * dpms on'";
      }
    ];
  };

  xdg.desktopEntries =
    let
      ocr = lang: name: {
        name = "OCR image: ${lang}";
        exec = "${pkgs.writeScript "ocr${name}" /*bash*/ ''
          ${pkgs.grim}/bin/grim -g "$(${pkgs.slurp}/bin/slurp || exit)" - \
            | ${pkgs.tesseract}/bin/tesseract -l ${lang} - - \
            | ${pkgs.wl-clipboard}/bin/wl-copy
        ''}";
      };
      scrot = args: name: {
        name = "Screenshot: ${name}";
        exec = "${pkgs.writeScript "scrot${name}" /*bash*/ ''
          scrot.fish ${args}
        ''}";
      };
      nmreload = {
        name = "Reload Network Manager";
        exec = "${pkgs.writeScript "nmreload" /*bash*/ ''
          ${pkgs.networkmanager}/bin/nmcli device wifi rescan
        ''}";
      };
    in
    {
      ocr-en = ocr "eng" "en";
      ocr-ru = ocr "rus" "ru";
      ocr-pl = ocr "pol" "pl";
      scrot-screen = scrot "-ts" "screen";
      scrot-window = scrot "-tw" "window";
      scrot-interactive = scrot "-ti" "interactive";
      inherit nmreload;
    };

  programs.swaylock = {
    enable = true;
    settings = {
      image = "${../../../wallpapers/Windows11BSOD.jpg}";
      scaling = "fill";
    };
  };

  home.packages = with pkgs; [
    swaylock
    swayidle
    waybar
    swaynotificationcenter
    grim
    slurp
    wl-clipboard
    rofi-wayland
    qt5.qtwayland
    qt6.qtwayland
    adwaita-qt
    glib
    wev
    wf-recorder
    nixpkgs2405.kooha
    autotiling
    wl-mirror
    dotool
  ];
}
