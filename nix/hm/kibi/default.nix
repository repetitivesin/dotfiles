{ config, pkgs, ... }:

{
  imports = [
    ../_conf/nvim.nix
    ../_conf/music.nix
    ../_conf/fonts.nix
    ../_conf/shared.nix
    ../_conf/sway.nix
    ../_conf/ssh.nix
  ];

  programs.fish.enable = true;
  programs.thunderbird = {
    enable = true;
    profiles = { };
  };

  home.packages = with pkgs; [
    eww
    wineWowPackages.full

    arduino-cli
    arduino-language-server
    arduino-ide
  ];

  home.pointerCursor = {
    package = pkgs.breeze-gtk;
    name = "breeze_cursors";
    # x11.enable = true;
    gtk.enable = true;
    size = 32;
  };

  gtk = {
    enable = false;
    theme = {
      package = pkgs.gnome-themes-extra;
      name = "Adwaita";
    };
    iconTheme = {
      package = pkgs.adwaita-icon-theme;
      name = "Adwaita";
    };

    gtk3.extraConfig = {
      gtk-primary-button-warps-slider = true;
    };
    gtk3.bookmarks = [
      "file:///${config.home.homeDirectory}/Documents/Sync/zk"
      "file://${config.home.homeDirectory}/Documents/Sync"
    ];
    gtk4.extraConfig = {
      gtk-primary-button-warps-slider = true;
    };
  };

  # home.file = {
  #   ".icons/default" = {
  #     source = "${pkgs.adwaita-icon-theme}/share/icons/Adwaita";
  #     recursive = true;
  #   };
  # };

  qt = {
    enable = false;
    platformTheme.name = "adwaita";
    # platformTheme = "gtk";
    style = {
      package = pkgs.adwaita-qt;
      name = "adwaita";
    };
  };

  services.syncthing.enable = true;
  services.blueman-applet.enable = true;
  services.udiskie.enable = false; # requires services.udisks2 to be enabled in configuration.nix

  xdg.configFile =
    let
      dotfiles = config.lib.file.mkOutOfStoreSymlink
        "${config.home.homeDirectory}/dotfiles";
    in
    {
      "clangd".source = "${dotfiles}/config/clangd";
      "easyeffects".source = "${dotfiles}/config/easyeffects";
      "eww".source = "${dotfiles}/config/eww";
      "fish/completions" = { source = "${dotfiles}/config/fish/completions"; };
      "fish/conf.d" = { source = "${dotfiles}/config/fish/conf.d"; };
      "fish/fish_plugins" = { source = "${dotfiles}/config/fish/fish_plugins"; };
      "fish/fish_variables" = { source = "${dotfiles}/config/fish/fish_variables"; };
      "fish/functions" = { source = "${dotfiles}/config/fish/functions"; };
      "ghc/ghci.conf".source = "${dotfiles}/config/ghc/ghci.conf";
      "kitty".source = "${dotfiles}/config/kitty";
      "lf".source = "${dotfiles}/config/lf";
      "mpv".source = "${dotfiles}/config/mpv";
      "nix".source = "${dotfiles}/config/nix";
      "nvim".source = "${dotfiles}/config/nvim";
      "rofi".source = "${dotfiles}/config/rofi";
      "sway/01config".source = "${dotfiles}/config/sway/01config";
      "swaync".source = "${dotfiles}/config/swaync";
      "wallpapers".source = "${dotfiles}/wallpapers";
      "waybar".source = "${dotfiles}/config/waybar";
      "zathura".source = "${dotfiles}/config/zathura";
    };

  home.file =
    let
      dotfiles = config.lib.file.mkOutOfStoreSymlink
        "${config.home.homeDirectory}/dotfiles";
    in
    {
      ".local/bin".source = "${dotfiles}/bin";
      ".haskeline".source = "${dotfiles}/config/haskeline.conf";
    };
}
