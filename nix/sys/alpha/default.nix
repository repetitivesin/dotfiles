# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports = [
    ./hardware-configuration.nix
    ../shared.nix
    ./transmission.nix
    ./samba.nix
    ./minecraft.nix
  ];

  services.xserver = {
    xkb.layout = "us";
    xkb.variant = "dvp";
  };
  console.useXkbConfig = true; # use xserver's keyboard configuration for TTY

  services.fstrim.enable = true;
  services.openssh = {
    enable = true;
    openFirewall = true;
    knownHosts = {
      t14s.publicKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBWkumnW8KWDBpbcYxoJ12+ZqB8qQtRhNgzLG5Ozk9Ko";
    };
    settings = {
      PasswordAuthentication = false;
      PermitRootLogin = "no";
    };
  };

  networking.firewall.enable = true;
  networking.firewall.allowedTCPPorts = [25565];

  users.users.tcs.openssh.authorizedKeys.keys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBWkumnW8KWDBpbcYxoJ12+ZqB8qQtRhNgzLG5Ozk9Ko kibi@t14s"
  ];

  # networking.interfaces."enp1s0f0".ipv4.addresses = [{
  #   address = "192.168.122.156";
  #   prefixLength = 24;
  # }];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "thinkcentre-alpha"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Enable networking
  networking.networkmanager.enable = true;

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "pl_PL.UTF-8";
    LC_IDENTIFICATION = "pl_PL.UTF-8";
    LC_MEASUREMENT = "pl_PL.UTF-8";
    LC_MONETARY = "pl_PL.UTF-8";
    LC_NAME = "pl_PL.UTF-8";
    LC_NUMERIC = "pl_PL.UTF-8";
    LC_PAPER = "pl_PL.UTF-8";
    LC_TELEPHONE = "pl_PL.UTF-8";
    LC_TIME = "pl_PL.UTF-8";
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.tcs = {
    isNormalUser = true;
    description = "tcs";
    extraGroups = [ "networkmanager" "wheel" "transmission" ];
    packages = with pkgs; [ ];
  };

  home-manager.users.tcs = { pkgs, ... }: {
    home = {
      packages = with pkgs; [
        vim
      ];
      inherit (config.system) stateVersion;
      sessionVariables = {
        EDITOR = "vim";
        VISUAL = "vim";
      };
    };
    programs.bash.enable = true;
  };

  environment.systemPackages = with pkgs; [
    sops
    age
    vim
  ];
}
