{ config, pkgs, lib, ... }:

{
  services.minecraft-server = {
    enable = true;
    eula = true;
    package = pkgs.minecraftServers.vanilla-1-7;
    declarative = true;
    openFirewall = true;
    serverProperties = {
      server-port = 43000;
      difficulty = 1;
      gamemode = 1;
      max-players = 5;
      motd = "NixOS Minecraft server!";
      online-mode = false;
    };
  };
}
