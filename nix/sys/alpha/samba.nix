{ pkgs, lib, config, ... }:

{
  users.users.samba-guest = {
    isSystemUser = true;
    group = "samba-guest";
  };
  users.users.samba-transmission = {
    isSystemUser = true;
    group = "samba-transmission";
    extraGroups = [ config.users.users.transmission.group ];
  };
  users.users.samba-nas = {
    isSystemUser = true;
    group = "samba-nas";
  };
  sops.secrets."samba/samba-transmission-password" = {
    owner = config.users.users.root.name;
  };
  sops.secrets."samba/samba-nas-password" = {
    owner = config.users.users.root.name;
  };
  system.activationScripts = {
    sambaGuestAccount = /*bash*/ ''
      ${pkgs.samba}/bin/smbpasswd -a samba-guest -n
    '';
    sambaTranmsissionAccount =
      let
        samba-pass-file =
          config.sops.secrets."samba/samba-transmission-password".path;
      in
        /*bash*/ ''
        printf "%s\n%s" \
          "$(cat '${samba-pass-file}')" \
          "$(cat '${samba-pass-file}')" \
          | ${pkgs.samba}/bin/smbpasswd -s -a samba-transmission
      '';
    sambaNasAccount =
      let
        samba-pass-file =
          config.sops.secrets."samba/samba-nas-password".path;
      in
        /*bash*/ ''
        printf "%s\n%s" \
          "$(cat '${samba-pass-file}')" \
          "$(cat '${samba-pass-file}')" \
          | ${pkgs.samba}/bin/smbpasswd -s -a samba-nas
      '';
  };
  users.groups.samba-guest = { };
  users.groups.samba-transmission = { };
  users.groups.samba-nas = { };
  services.samba = {
    enable = true;
    openFirewall = true;
    securityType = "user";
    settings.global = /*ini*/ {
      "workgroup" = "WORKGROUP";
      "server string" = "smbnix";
      "netbios name" = "smbnix";
      "security" = "user";
      "hosts allow" = "192.168.0. 127.0.0.1 localhost";
      "hosts deny" = "0.0.0.0/0";
      "guest account" = "samba-guest";
      "map to guest" = "bad user";
      "encrypt passwords" = "true";
      "min protocol" = "SMB3";
    };
    shares = {
      transmission = {
        "path" = "/var/lib/transmission/Downloads";
        "browseable" = "yes";
        "read only" = "yes";
        "guest ok" = "yes";
        "guest only" = "yes";
        "directory mask" = "0775";
      };
      transmission-rw = {
        "path" = "/var/lib/transmission/Downloads";
        "browseable" = "no";
        "read only" = "no";
        "create mask" = "0775";
        "directory mask" = "0775";
        "server smb encrypt" = "required";
        "valid users" = "${config.users.users.samba-transmission.name}";
      };
      nas = {
        "path" = "/run/media/winas";
        "read only" = "no";
        "create mask" = "0644";
        "directory mask" = "770";
        "server smb encrypt" = "required";
        "valid users" = "${config.users.users.samba-nas.name}";
      };
    };
  };
}
