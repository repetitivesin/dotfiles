{ config, lib, pkgs, ... }:

{
  # Make everyone be able to view /var/lib/transmission, including samba guest
  # account
  systemd.services.transmission.serviceConfig.StateDirectoryMode = 775;

  sops.secrets."transmission/rpc-password" = {
    owner = config.users.users.transmission.name;
  };

  sops.templates."transmission_password.json" = {
    content = builtins.toJSON {
      rpc-password = "${config.sops.placeholder."transmission/rpc-password"}";
    };
  };

  sops.secrets."transmission.pem" = {
    sopsFile = ../../../secrets/transmission.pem;
    format = "binary";
    owner = "hitch";
  };

  sops.secrets."wireguard-wg0/interface-private-key" = {
    owner = config.users.users.root.name;
    restartUnits = [ "wg-quick-wg0.service" ];
  };

  sops.secrets."wireguard-wg0/peer-preshared-key" = {
    owner = config.users.users.root.name;
    restartUnits = [ "wg-quick-wg0.service" ];
  };

  services.hitch = {
    enable = true;
    backend = "[127.0.0.1]:9090";
    frontend = "[0.0.0.0]:9091";
    pem-files = [ config.sops.secrets."transmission.pem".path ];
    # extraConfig = /* ini */ ''
    #   log-level = 2
    #   syslog = "on"
    # '';
  };

  networking.firewall = {
    logReversePathDrops = true;
    extraCommands = /*bash*/ ''
      # makes wireguard work
      # see https://discourse.nixos.org/t/solved-minimal-firewall-setup-for-wireguard-client/7577/2
      ip46tables -t mangle -I nixos-fw-rpfilter -p udp -m udp --sport 51820 -j RETURN
      ip46tables -t mangle -I nixos-fw-rpfilter -p udp -m udp --dport 51820 -j RETURN

      ## Open port 9091 for transmission
      iptables -I nixos-fw -p tcp --dport 9091 -s 192.168.0.0/16 -j nixos-fw-accept
      ip46tables -I nixos-fw -p udp --dport 53769 -i wg0 -j nixos-fw-accept
      ip46tables -I nixos-fw -p tcp --dport 53769 -i wg0 -j nixos-fw-accept
    '';
    extraStopCommands = /*bash*/ ''
      # see https://discourse.nixos.org/t/solved-minimal-firewall-setup-for-wireguard-client/7577/2
      ip46tables -t mangle -D nixos-fw-rpfilter -p udp -m udp --sport 51820 -j RETURN || true
      ip46tables -t mangle -D nixos-fw-rpfilter -p udp -m udp --dport 51820 -j RETURN || true
      iptables -D nixos-fw -p tcp --dport 9091 -s 192.168.0.0/16 -j nixos-fw-accept || true
      ip46tables -D nixos-fw -p udp --dport 53769 -i wg0 -j nixos-fw-accept || true
      ip46tables -D nixos-fw -p tcp --dport 53769 -i wg0 -j nixos-fw-accept || true
    '';
  };

  networking.wg-quick.interfaces.wg0 =
    let
      i = "wg0";
      uid = config.users.users.transmission.name;
    in
    {
      address = [ "10.160.18.126,fd7d:76ee:e68f:a993:e45e:f5a9:314f:f6b5" ];
      privateKeyFile = config.sops.secrets."wireguard-wg0/interface-private-key".path;
      mtu = 1320;
      dns = [ "10.128.0.1, fd7d:76ee:e68f:a993::1" ];
      postUp = /*bash*/ ''
        set -x
        iptables -I OUTPUT ! -o ${i} \
            -m mark ! --mark $(wg show ${i} fwmark) \
            -m addrtype ! --dst-type LOCAL \
            -m owner --uid-owner ${uid} -j nixos-fw-log-refuse && \
        ip6tables -I OUTPUT ! -o ${i} \
            -m mark ! --mark $(wg show ${i} fwmark) \
            -m addrtype ! --dst-type LOCAL \
            -m owner --uid-owner ${uid} -j nixos-fw-log-refuse
      '';
      preDown = /*bash*/ ''
        iptables -D OUTPUT ! -o ${i} \
            -m mark ! --mark $(wg show ${i} fwmark) \
            -m addrtype ! --dst-type LOCAL \
            -m owner --uid-owner ${uid} -j nixos-fw-log-refuse && \
        ip6tables -D OUTPUT ! -o ${i} \
            -m mark ! --mark $(wg show ${i} fwmark) \
            -m addrtype ! --dst-type LOCAL \
            -m owner --uid-owner ${uid} -j nixos-fw-log-refuse
      '';
      peers = [
        {
          publicKey = "PyLCXAQT8KkM4T+dUsOQfn+Ub3pGxfGlxkIApuig+hk=";
          presharedKeyFile = config.sops.secrets."wireguard-wg0/peer-preshared-key".path;
          endpoint = "37.120.217.245:1637";
          allowedIPs = [ "0.0.0.0/0" "::/0" ];
          persistentKeepalive = 15;
        }
      ];
    };

  # DNS resolution fails when starting wg-quick-wg0.service
  # This is old issue (yet to be resolved)
  # The best solution to date is
  # <https://github.com/NixOS/nixpkgs/issues/30459#issuecomment-495083501>
  # also relevant: <https://github.com/NixOS/nixpkgs/issues/63869>
  systemd.services.wg-quick-wg0.serviceConfig = {
    Restart = "on-failure";
    RestartSec = "5s";
    Type = lib.mkForce "simple";
  };

  services.transmission = {
    enable = true;
    downloadDirPermissions = "775";
    package = pkgs.transmission_4;
    # NOTE: you can specify all settings that
    # /var/lib/transmission/.config/transmission-daemon/settings.json supports
    settings = {
      umask = 2; # 002
      rpc-port = 9090;
      rpc-bind-address = "127.0.0.1";
      rpc-whitelist = "127.0.0.1";
      rpc-authentication-required = true;
      rpc-username = "user";
      peer-port = 53769;
    };
    credentialsFile = "${config.sops.templates."transmission_password.json".path}";
  };
}
