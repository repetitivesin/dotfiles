{ pkgs, lib, ... }:

{
  services.xserver = {
    xkb = {
      layout = "us";
      variant = "dvp";
      options = "grp:win_space_toggle,compose:ralt";
    };
    autoRepeatDelay = 400;
    autoRepeatInterval = 60;

  };
  services.libinput = {
    enable = true;
    touchpad = {
      accelProfile = "flat";
      clickMethod = "buttonareas";
      disableWhileTyping = true;
      middleEmulation = true;
      naturalScrolling = true;
    };
  };
  console.keyMap = "dvorak-programmer";

  services.kanata = {
    enable = true;
    keyboards._.configFile = ../../config/kanata/config.kbd;
  };

  hardware.graphics = {
    enable = lib.mkDefault true;
    enable32Bit = lib.mkDefault true;
  };

  boot = {
    initrd.verbose = false;
    plymouth.enable = true;
    kernelParams = [
      "quiet"
      "splash"
      "rd.systemd.show_status=false"
      "rd.udev.log_level=3"
      "udev.log_priority=3"
      "boot.shell_on_fail"
    ];
  };

  services.pipewire = {
    enable = lib.mkDefault true;
    alsa.enable = lib.mkDefault true;
    alsa.support32Bit = lib.mkDefault true;
    pulse.enable = lib.mkDefault true;
  };

  security.rtkit.enable = lib.mkDefault true;
  security.polkit.enable = lib.mkDefault true;
  programs.dconf.enable = lib.mkDefault true;

  environment.systemPackages = with pkgs; [
    cachix
    git
    git-crypt
    home-manager
    exfatprogs
    libnotify
    alsa-utils
    xkeyboard_config

    # Android
    android-file-transfer
    scrcpy
    payload_dumper

    # XDG
    xdg-user-dirs
    xdg-utils

    # spellchecking
    hunspell
    hunspellDicts.en-us-large
    hunspellDicts.en-us
    hunspellDicts.ru-ru
    hunspellDicts.pl-pl
  ];

  programs.adb.enable = true;
}
