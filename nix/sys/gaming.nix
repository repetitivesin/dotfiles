{ inputs, pkgs, lib, config, ... }:

{
  programs.steam = {
    enable = lib.mkDefault true;
    # extest.enable = true;
    # extraPackages = with pkgs; [
    #   gamescope
    # ];
    # gamescopeSession.enable = true;
  };
  services = {
    desktopManager.plasma6.enable = lib.mkDefault true;
    power-profiles-daemon.enable = !(
      config.services.throttled.enable or
        config.services.tlp.enable
    );
  };

  environment.plasma6.excludePackages = with pkgs.kdePackages; [
    elisa
    gwenview
    okular
  ];
  services.displayManager.sddm.enable = lib.mkDefault false;

  users.users.player = {
    isNormalUser = true;
    description = "player";
    extraGroups = [
      "networkmanager"
      "video"
    ];
    shell = pkgs.fish;
    homeMode = "770";
    initialHashedPassword = "!";
  };

  programs.java = {
    enable = true;
    package = pkgs.jetbrains.jdk;
    binfmt = true;
  };

  programs.gamescope = {
    enable = true;
    # capSysNice = true;
  };

  home-manager.useGlobalPkgs = true;
  home-manager.users.player = { pkgs, ... }: {
    home.packages = with pkgs; [
      neovim
      lf
      librewolf
      steam-run
      yad
      pv
      cantata
      telegram-desktop
      winetricks
      unstable.wineWowPackages.unstableFull
      dxvk
      protontricks
      protonup
      # bottles
      lutris
      antimicrox
      p7zip
      mangohud
      distrobox
    ];
    home.stateVersion = config.system.stateVersion;
    programs.fish = {
      enable = true;
      shellInit = /* fish */ ''
        umask 0007
        fish_hybrid_key_bindings
      '';
    };
    programs.fzf = {
      enable = true;
      enableFishIntegration = true;
    };
    nixpkgs.config.allowUnfree = true;
  };
}
