{ pkgs, inputs, lib, config, ... }:

{
  time.timeZone = lib.mkDefault "Europe/Warsaw";
  i18n.defaultLocale = lib.mkDefault "en_IE.UTF-8";

  # <https://nixos.wiki/wiki/Encrypted_DNS>
  networking = {
    nameservers = [
      "127.0.0.1" ## DNS is provided by dnscrypt-proxy
      "::1"
    ];
    dhcpcd.extraConfig = "nohook resolv.conf";
    networkmanager.dns = "none";
  };

  services.dnscrypt-proxy2 = {
    enable = true;
    settings = {
      ipv4_servers = true;
      ipv6_servers = true;
      doh_servers = true;
      require_nofilter = false;
      require_dnssec = true;
      server_names = [ "dns0" "cloudflare" "adguard-dns" ];
      listen_addresses = [ "127.0.0.1:53" "[::1]:53" ];
    };
  };

  boot.tmp.useTmpfs = lib.mkDefault true;
  boot.loader.systemd-boot.editor = lib.mkDefault false;

  security.doas = {
    enable = lib.mkDefault true;
    extraRules = [{
      groups = [ "wheel" ];
      persist = true;
      keepEnv = false;
    }];
  };

  security.sudo.enable = lib.mkDefault false;

  nix = {
    # This will add each flake input as a registry
    # To make nix3 commands consistent with your flake
    registry = lib.mapAttrs (_: value: { flake = value; }) inputs;

    # This will additionally add your inputs to the system's legacy channels
    # Making legacy nix commands consistent as well, awesome!
    nixPath = lib.mapAttrsToList (key: value: "${key}=${value.to.path}") config.nix.registry;
    extraOptions = /*ini*/ ''
      experimental-features = nix-command flakes
      # keep-outputs = true
      # keep-derivations = true
    '';
    settings = {
      auto-optimise-store = true;
      trusted-users = [ "root" "@wheel" ];
      trusted-public-keys = [
        "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
      ];
      substituters = [
        "https://nix-community.cachix.org"
      ];
    };
  };

  environment.systemPackages = with pkgs; [
    vim
    git
    wget
    dash
    pciutils
    usbutils
    openssl
    (lib.mkIf (!config.security.sudo.enable)
      (pkgs.writeScriptBin "sudo" ''
        exec doas "$@"
      ''))
  ];

  users.users.root.hashedPassword = "!";
}
