# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, lib, pkgs, ... }:

{
  imports =
    [
      ./hardware-configuration.nix # Include the results of the hardware scan.
      ../desktop.nix
      ../shared.nix
      ../virtualization.nix
      ../gaming.nix
    ];

  boot = {
    loader = {
      systemd-boot.enable = lib.mkForce false;
      timeout = 1;
      efi.canTouchEfiVariables = true;
    };
    # kernelParams = [ "memtest=20" ]; # if ram is faulty but useful
    kernelModules = [ "amdgpu" "acpi_call" ];
    kernelPackages = pkgs.unstable.linuxPackages_latest;
    extraModulePackages = with config.boot.kernelPackages; [ acpi_call ];
    extraModprobeConfig = ''
      options psmouse synaptics_intertouch=1
    '';
  };

  boot.lanzaboote = {
    enable = true;
    pkiBundle = "/etc/secureboot";
  };

  services.xserver.videoDrivers = [ "amdgpu" ];
  networking = {
    hostName = "t14s"; # Define your hostname.
    networkmanager = {
      enable = true;
      wifi = {
        powersave = true;
        macAddress = "random";
      };
    };
    firewall = {
      allowedTCPPorts = [ ];
      allowedUDPPorts = [ ];
      logReversePathDrops = true;
      extraCommands = /*bash*/ ''
        # makes wireguard work
        # see https://discourse.nixos.org/t/solved-minimal-firewall-setup-for-wireguard-client/7577/2
        # see https://nixos.wiki/wiki/WireGuard#Setting_up_WireGuard_with_NetworkManager
        # NOTE: To make wireguard note with networkmanager, set target port to
        # 51820 and *NOT* automatic
        ip46tables -t mangle -I nixos-fw-rpfilter -p udp -m udp --sport 51820 -j RETURN
        ip46tables -t mangle -I nixos-fw-rpfilter -p udp -m udp --dport 51820 -j RETURN
      '';
      extraStopCommands = /*bash*/ ''
        # see https://discourse.nixos.org/t/solved-minimal-firewall-setup-for-wireguard-client/7577/2
        ip46tables -t mangle -D nixos-fw-rpfilter -p udp -m udp --sport 51820 -j RETURN || true
        ip46tables -t mangle -D nixos-fw-rpfilter -p udp -m udp --dport 51820 -j RETURN || true
      '';
    };
  };

  environment.systemPackages = with pkgs; [
    sops
    ntfs3g
    pkgs.cifs-utils
    sbctl
  ];

  xdg.portal = {
    enable = true;
    wlr.enable = true;
    # gtk portal needed to make gtk apps happy
    extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
  };

  programs.nm-applet = {
    enable = true;
    indicator = true;
  };

  environment.sessionVariables = {
    _JAVA_AWT_WM_NONREPARENTING = "1";
    SDL_VIDEODRIVER = "wayland";
    QT_QPA_PLATFORM = "wayland";
    ANKI_WAYLAND = "1";
    DISABLE_QT5_COMPAT = "1";
  };

  programs.wshowkeys.enable = true;
  programs.sway = {
    enable = true;
    wrapperFeatures.gtk = true;
    package = pkgs.sway;
    extraPackages = with pkgs.unstable; [ wmenu ];
  };

  programs.wireshark.enable = true;

  programs.light.enable = true;
  programs.xwayland.enable = true;
  services.fwupd.enable = true;

  services.greetd = {
    enable = true;
    vt = 2;
    settings = {
      default_session = {
        command = "${pkgs.greetd.tuigreet}/bin/tuigreet --time --cmd sway";
        user = "greeter";
      };
    };
  };

  services.udisks2.enable = true;


  systemd.tmpfiles.rules = [
    "L+    /opt/rocm/hip   -    -    -     -    ${pkgs.rocmPackages.clr}"
  ];
  hardware.graphics = {
    extraPackages = with pkgs; [
      rocmPackages.clr.icd
      amdvlk
    ];
    extraPackages32 = with pkgs; [
      driversi686Linux.amdvlk
    ];

  };

  ### https://github.com/NixOS/nixos-hardware/blob/03c6d1515228ff524d63f532a2a6709dfd561a70/lenovo/thinkpad/t480/default.nix
  hardware.trackpoint = {
    enable = true;
    sensitivity = 255;
  };
  services.fstrim.enable = true;
  services.smartd.enable = true;
  services.tlp.enable = true;
  services.power-profiles-daemon.enable = lib.mkForce false;
  zramSwap.enable = true;
  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.kibi = {
    uid = 1000;
    isNormalUser = true;
    description = "kibi";
    extraGroups = with config.users.groups; [
      wireshark.name
      libvirtd.name
      kvm.name
      networkmanager.name
      wheel.name
      video.name
      input.name
      adbusers.name
    ];
    shell = pkgs.fish;
  };

  services.udev.extraRules = ''
    ACTION=="add", KERNEL=="ttyUSB[0-9]*", ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6001", MODE="0666"
  '';

  programs.fish.enable = true;

  hardware.bluetooth = {
    enable = true;
    powerOnBoot = false;
  };

  services.blueman.enable = true;

  services.flatpak.enable = true;

  fileSystems."/run/media/transmission" = {
    device = "//192.168.0.41/transmission";
    fsType = "cifs";
    options = [
      "guest"
      "x-systemd.automount"
      "noauto"
      "x-systemd.idle-timeout=60"
      "x-systemd.device-timeout=5s"
      "x-systemd.mount-timeout=5s"
    ];
  };

  sops.secrets."samba/samba-nas-password" = {
    owner = config.users.users.root.name;
  };

  sops.templates."samba-nas" = {
    content = ''
      username=samba-nas
      password=${config.sops.placeholder."samba/samba-nas-password"}
    '';
  };

  fileSystems."/run/media/samba-nas" = {
    device = "//192.168.0.41/nas";
    fsType = "cifs";
    options = [
      "credentials=${config.sops.templates."samba-nas".path}"
      "x-systemd.automount"
      "noauto"
      "x-systemd.idle-timeout=60"
      "x-systemd.device-timeout=5s"
      "x-systemd.mount-timeout=5s"
      "rw"
      "uid=${builtins.toString config.users.users.kibi.uid}"
      "gid=${builtins.toString config.users.groups.users.gid}"
    ];
  };
}
