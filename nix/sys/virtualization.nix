{ pkgs, ... }:

{
  # https://www.reddit.com/r/NixOS/comments/177wcyi/comment/k4vok4n
  virtualisation = {
    libvirtd.enable = true;
    libvirtd.qemu.vhostUserPackages = with pkgs; [ virtiofsd ];
    spiceUSBRedirection.enable = true;
    podman.enable = true;
    # virtualbox = {
    #   host.enable = true;
    #   # guest.enable = true;
    #   # guest.x11 = true;
    # };
  };
  programs.dconf.enable = true;
  environment.systemPackages = with pkgs; [
    virt-manager
    win-virtio
    virtiofsd
  ];
}
